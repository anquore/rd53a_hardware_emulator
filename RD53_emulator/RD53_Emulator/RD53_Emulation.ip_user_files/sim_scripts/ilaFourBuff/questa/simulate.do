onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib ilaFourBuff_opt

do {wave.do}

view wave
view structure
view signals

do {ilaFourBuff.udo}

run -all

quit -force
