onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib triggerTagFifo_opt

do {wave.do}

view wave
view structure
view signals

do {triggerTagFifo.udo}

run -all

quit -force
