onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib ilaHit_opt

do {wave.do}

view wave
view structure
view signals

do {ilaHit.udo}

run -all

quit -force
