vlib work

vlog -work work ../src/off_chip/triggerunit.v
vlog -work work trigger_unit_tb.sv
 
vsim -t 1pS -novopt trigger_unit_tb

view signals
view wave

do wave_trigger_unit.do

run -all
