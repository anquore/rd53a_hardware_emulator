onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk200
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk200_n
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk200_p
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk160_n
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk160_p
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk160
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk40
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk400
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk640
add wave -noupdate -expand -group {TB Clocks} /emulator_tb/clk1280
add wave -noupdate /emulator_tb/Emulator/clk160
add wave -noupdate /emulator_tb/Emulator/clk40_i
add wave -noupdate /emulator_tb/Emulator/clk40
add wave -noupdate /emulator_tb/Emulator/clk320
add wave -noupdate /emulator_tb/Emulator/clk640
add wave -noupdate /emulator_tb/Emulator/rst
add wave -noupdate -color Yellow -label {Data to TTC} -radix hexadecimal -childformat {{{/emulator_tb/datareg[15]} -radix hexadecimal} {{/emulator_tb/datareg[14]} -radix hexadecimal} {{/emulator_tb/datareg[13]} -radix hexadecimal} {{/emulator_tb/datareg[12]} -radix hexadecimal} {{/emulator_tb/datareg[11]} -radix hexadecimal} {{/emulator_tb/datareg[10]} -radix hexadecimal} {{/emulator_tb/datareg[9]} -radix hexadecimal} {{/emulator_tb/datareg[8]} -radix hexadecimal} {{/emulator_tb/datareg[7]} -radix hexadecimal} {{/emulator_tb/datareg[6]} -radix hexadecimal} {{/emulator_tb/datareg[5]} -radix hexadecimal} {{/emulator_tb/datareg[4]} -radix hexadecimal} {{/emulator_tb/datareg[3]} -radix hexadecimal} {{/emulator_tb/datareg[2]} -radix hexadecimal} {{/emulator_tb/datareg[1]} -radix hexadecimal} {{/emulator_tb/datareg[0]} -radix hexadecimal}} -subitemconfig {{/emulator_tb/datareg[15]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[14]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[13]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[12]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[11]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[10]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[9]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[8]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[7]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[6]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[5]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[4]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[3]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[2]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[1]} {-color Yellow -height 15 -radix hexadecimal} {/emulator_tb/datareg[0]} {-color Yellow -height 15 -radix hexadecimal}} /emulator_tb/datareg
add wave -noupdate /emulator_tb/datareg
add wave -noupdate -color Yellow -label {TTC Data} /emulator_tb/ttc_data
add wave -noupdate /emulator_tb/ttc_data
add wave -noupdate -color Yellow -label {TTC MMCM Locked} /emulator_tb/Emulator/mmcm_locked
add wave -noupdate /emulator_tb/Emulator/mmcm_locked
add wave -noupdate -color Yellow -label {CLK 80 PLL Locked} /emulator_tb/Emulator/cout_i/pll_locked
add wave -noupdate -color Yellow -label {CLK 80} /emulator_tb/Emulator/cout_i/clk80
add wave -noupdate -color Yellow -label CalDONE -radix hexadecimal /emulator_tb/Emulator/cout_i/hit_gen/calDone
add wave -noupdate -color Cyan -label {Trigger Out} /emulator_tb/Emulator/trig_out
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_in}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/sync}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_out_p}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_out_n}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/data32_gb_tx}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/data_next}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/gearbox_rdy}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/data66_tx_scr}
add wave -noupdate -expand -group {Tx Core 0} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[0]/tx_lane/piso}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_in}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/sync}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_out_p}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_out_n}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/data32_gb_tx}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/data_next}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/gearbox_rdy}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/data66_tx_scr}
add wave -noupdate -group {Tx Core 1} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[1]/tx_lane/piso}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_in}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/sync}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_out_p}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_out_n}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/data32_gb_tx}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/data_next}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/gearbox_rdy}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/data66_tx_scr}
add wave -noupdate -group {Tx Core 2} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[2]/tx_lane/piso}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_in}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/sync}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_out_p}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_out_n}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data32_gb_tx}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data_next}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/gearbox_rdy}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data66_tx_scr}
add wave -noupdate -expand -group {Tx Core 3} -color Salmon {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/piso}
add wave -noupdate -expand -group {Chip Out} /emulator_tb/Emulator/data_next
add wave -noupdate -expand -group {Chip Out} -label {data_next OR} /emulator_tb/Emulator/cout_i/data_next
add wave -noupdate -expand -group {Chip Out} /emulator_tb/Emulator/cout_i/frame_out
add wave -noupdate -expand -group {Chip Out} /emulator_tb/Emulator/cout_i/service_frame
add wave -noupdate -expand -group {Command Out} /emulator_tb/Emulator/cout_i/rst
add wave -noupdate -expand -group {Command Out} /emulator_tb/Emulator/cout_i/data_out
add wave -noupdate -expand -group {Command Out} /emulator_tb/Emulator/cout_i/data_out_valid
add wave -noupdate -expand -group {Command Out} /emulator_tb/Emulator/cout_i/service_frame_out
add wave -noupdate -expand -group {Command Out} /emulator_tb/Emulator/cout_i/cmd_handler_i/data_available
add wave -noupdate -expand -group {Command Out} /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out_prereverse
add wave -noupdate -expand -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/hitData
add wave -noupdate -expand -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/next_hit
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/frame
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/service_frame
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/frame_valid
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/present_frame
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/frame_hold
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/service_hold
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/read_frame
add wave -noupdate -expand -group {Frame Buffer} /emulator_tb/Emulator/cout_i/buffer_i/present_frame_depress
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[123]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[119]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]}
add wave -noupdate /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR_C
add wave -noupdate -radix hexadecimal /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_in
add wave -noupdate -childformat {{{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]} -radix decimal} {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]} -radix decimal} {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]} -radix decimal}} -subitemconfig {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]} {-height 15 -radix decimal} {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]} {-height 15 -radix decimal} {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]} {-height 15 -radix decimal}} /emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/channel_bonded
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_valid_cb
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/sync_out_cb
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_out_cb
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} -expand -subitemconfig {{/emulator_tb/blocksync_out[3]} {-color {Slate Blue} -height 15} {/emulator_tb/blocksync_out[2]} {-color {Slate Blue} -height 15} {/emulator_tb/blocksync_out[1]} {-color {Slate Blue} -height 15} {/emulator_tb/blocksync_out[0]} {-color {Slate Blue} -height 15}} /emulator_tb/blocksync_out
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/gearbox_rdy_rx
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_valid
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_out
add wave -noupdate -expand -group {Rx Cores} -color {Slate Blue} /emulator_tb/sync_out
add wave -noupdate -expand -group {Rx Core 3} -color {Cadet Blue} {/emulator_tb/rx_core[3]/rx_lane/data_in_p}
add wave -noupdate -expand -group {Rx Core 3} -color {Cadet Blue} {/emulator_tb/rx_core[3]/rx_lane/data_in_n}
add wave -noupdate -expand -group {Rx Core 3} -color {Cadet Blue} {/emulator_tb/rx_core[3]/rx_lane/rxgearboxslip_out}
add wave -noupdate -expand -group {Rx Core 3} -color {Cadet Blue} {/emulator_tb/rx_core[3]/rx_lane/sync_out}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/clk}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/rst}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/blocksync}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/rxgearboxslip}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/iserdes_slip}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/gearbox_slip}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/iserdes_slip_cnt}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/gearbox_slip_cnt}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/slip_delay_cnt}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/iserdes_slip_cnt_next}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} -radix decimal {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/gearbox_slip_cnt_next}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/slip_delay_cnt_next}
add wave -noupdate -expand -group {Rx Core 3} -group {Bitslip FSM} -color {Indian Red} {/emulator_tb/rx_core[3]/rx_lane/bs_fsm/bitslip_state}
add wave -noupdate -expand -group {Rx Core 3} -group {Block Sync} -color Tan {/emulator_tb/rx_core[3]/rx_lane/b_sync/sync_header_count_i}
add wave -noupdate -expand -group {Rx Core 3} -group {Block Sync} -color Tan {/emulator_tb/rx_core[3]/rx_lane/b_sync/sync_header_invalid_count_i}
add wave -noupdate -expand -group {Rx Core 3} -group {Block Sync} -color Tan {/emulator_tb/rx_core[3]/rx_lane/b_sync/rxheader_in}
add wave -noupdate -expand -group {Rx Core 3} -group {Block Sync} -color Tan {/emulator_tb/rx_core[3]/rx_lane/b_sync/rxheadervalid_in}
add wave -noupdate -expand -group {Rx Core 3} -color {Cadet Blue} {/emulator_tb/rx_core[3]/rx_lane/sipo}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/rst}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/clk}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/data32}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/gearbox_slip}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/gearbox_rdy}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/data66}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/data_valid}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/cycle}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/counter}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/first_cycle}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/buffer_pos}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/buffer_128}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/rotate}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/left_shift}
add wave -noupdate -expand -group {Rx Core 3} -expand -group Gearbox -color {Medium Sea Green} {/emulator_tb/rx_core[3]/rx_lane/rx_gb/right_shift}
add wave -noupdate -expand -group {Rx Core 2} -color {Cadet Blue} {/emulator_tb/rx_core[2]/rx_lane/data_in_p}
add wave -noupdate -expand -group {Rx Core 2} -color {Cadet Blue} {/emulator_tb/rx_core[2]/rx_lane/data_in_n}
add wave -noupdate -expand -group {Rx Core 2} -color {Cadet Blue} {/emulator_tb/rx_core[2]/rx_lane/rxgearboxslip_out}
add wave -noupdate -expand -group {Rx Core 2} -color {Cadet Blue} {/emulator_tb/rx_core[2]/rx_lane/sync_out}
add wave -noupdate -expand -group {Rx Core 2} -color {Cadet Blue} {/emulator_tb/rx_core[2]/rx_lane/sipo}
add wave -noupdate -expand -group {Rx Core 1} -color {Cadet Blue} {/emulator_tb/rx_core[1]/rx_lane/data_in_p}
add wave -noupdate -expand -group {Rx Core 1} -color {Cadet Blue} {/emulator_tb/rx_core[1]/rx_lane/data_in_n}
add wave -noupdate -expand -group {Rx Core 1} -color {Cadet Blue} {/emulator_tb/rx_core[1]/rx_lane/rxgearboxslip_out}
add wave -noupdate -expand -group {Rx Core 1} -color {Cadet Blue} {/emulator_tb/rx_core[1]/rx_lane/sync_out}
add wave -noupdate -expand -group {Rx Core 1} -color {Cadet Blue} {/emulator_tb/rx_core[1]/rx_lane/sipo}
add wave -noupdate -expand -group {Rx Core 0} -color {Cadet Blue} {/emulator_tb/rx_core[0]/rx_lane/data_in_p}
add wave -noupdate -expand -group {Rx Core 0} -color {Cadet Blue} {/emulator_tb/rx_core[0]/rx_lane/data_in_n}
add wave -noupdate -expand -group {Rx Core 0} -color {Cadet Blue} {/emulator_tb/rx_core[0]/rx_lane/rxgearboxslip_out}
add wave -noupdate -expand -group {Rx Core 0} -color {Cadet Blue} {/emulator_tb/rx_core[0]/rx_lane/sync_out}
add wave -noupdate -expand -group {Rx Core 0} -color {Cadet Blue} {/emulator_tb/rx_core[0]/rx_lane/sipo}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1438557569 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 243
configure wave -valuecolwidth 176
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {1434368358 fs} {1456798778 fs}
