vlib work

vlog -work work ../src/on_chip/gearbox_66_to_32.sv
vlog -work work ./gbox_66_32_chip_tb.sv

vsim -t 1ps -novopt gbox66to32_tb

view signals
view wave

do wave_gbox_66_to_32.do

run -all