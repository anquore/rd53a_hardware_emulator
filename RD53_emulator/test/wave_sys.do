onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Green -label RST /sys_tb/rst
add wave -noupdate -color Green -label CLKIn40 /sys_tb/clk40
add wave -noupdate -color Green -label {Shift In} /sys_tb/shift_in
add wave -noupdate -color Green -label Trigger /sys_tb/trig
add wave -noupdate -color Yellow -label {Trigger Out} /sys_tb/dut/emulator/cout_i/trig_out
add wave -noupdate -color Green -label {Trigger In} /sys_tb/dut/ext/trig_ii
add wave -noupdate -color Green -label Command /sys_tb/cmd
add wave -noupdate -color Blue -label CLK40 /sys_tb/dut/ext/clk40
add wave -noupdate -color Blue -label CLK80 /sys_tb/dut/ext/clk80
add wave -noupdate -color Blue -label CLK160 /sys_tb/dut/ext/clk160
add wave -noupdate -color Blue -label {PLL Lock} /sys_tb/dut/ext/pll_locked
add wave -noupdate -color Blue -label {Lock Count} -radix unsigned /sys_tb/dut/ext/lock_count
add wave -noupdate -color Blue -label {Hold Till Lock} /sys_tb/dut/ext/hold_while_locking
add wave -noupdate -color Cyan -label {Encoded Trig} -radix hexadecimal /sys_tb/dut/ext/trig_gen/enc_trig
add wave -noupdate -color Cyan -label {Encoded Trig Ready} /sys_tb/dut/ext/trig_gen/trigger_rdy
add wave -noupdate -color Cyan -label {Command Data} -radix hexadecimal /sys_tb/dut/ext/cmd_gen/cmd_data
add wave -noupdate /sys_tb/dut/ext/cmd_gen/gen_cmd
add wave -noupdate /sys_tb/dut/ext/cmd_gen/wr_cmd
add wave -noupdate /sys_tb/dut/ext/cmd_gen/rd_cmd
add wave -noupdate -radix hexadecimal /sys_tb/dut/ext/cmd_gen/next_cmd
add wave -noupdate /sys_tb/dut/ext/cmd_gen/state
add wave -noupdate -color Cyan -label {Serial Data to TTC} -radix hexadecimal /sys_tb/dut/ext/ser_data_i
add wave -noupdate -color Cyan -label {TTC Data} /sys_tb/dut/emulator/ttc_data
add wave -noupdate -color Red -label {Post-TTC Data} -radix hexadecimal /sys_tb/dut/emulator/data_i
add wave -noupdate -color Red -label {Post-TTC Data Valid} /sys_tb/dut/emulator/valid_i
add wave -noupdate /sys_tb/dut/emulator/cout_i/process_cmd_in_i/dataword
add wave -noupdate -radix unsigned /sys_tb/dut/emulator/cout_i/process_cmd_in_i/state
add wave -noupdate -radix hexadecimal /sys_tb/dut/emulator/data_out
add wave -noupdate -radix hexadecimal /sys_tb/dut/emulator/data_out_valid
add wave -noupdate /sys_tb/dut/cmd_out_n
add wave -noupdate /sys_tb/dut/cmd_out_p
add wave -noupdate /sys_tb/trig_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {8751718 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 187
configure wave -valuecolwidth 81
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {19844140 ps} {20008204 ps}
