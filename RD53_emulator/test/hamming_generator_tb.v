module hamming_generator_tb ();

reg [49:0] seed;
wire [5:0] parity_bits;

hamming_generator uut (
    .seed(seed),
    .parity_bits(parity_bits)
);

initial seed <= 50'b0;

always #50 begin
    seed[49:32] <= $urandom % 262144;
    seed[31:0]  <= $urandom;
end

endmodule
