// Testbench: Gearbox + Scrambler + SERDES + Block Sync test
//
//                       Tx Gearbox            OSERDES              ISERDES            Rx Gearbox
//  +----------+        +----------+        +-----------+   8    +-----------+        +----------+        +----------+   2             2   +-------------+
//  |   Tx     |   66   |          |   32   |           | --/--> |           |   32   |          |   66   |   Rx     | --/-->  sync  --/-->| Block Sync  |
//  |Scrambler | --/--> | 66 to 32 | --/--> | 32 to 4x8 | --/--> | 4x8 to 32 | --/--> | 32 to 66 | --/--> |   De     |   64                +-------------+
//  |          |        |          |        |           | --/--> |           |        |          |        |Scrambler | --/-->  data
//  +----------+        +----------+        +-----------+ --/--> +-----------+        +----------+        +----------+
//

`timescale  1ns / 1ps

`define PROPER_HEADERS

module system_test_serdes_tb();

// Clocks
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;
//parameter clk640_period = 1.5625*2;

reg clk160;
reg clk640;

// Reset
reg rst;
reg rst_tx, rst_rx;

// Gearbox Signals
wire [31:0] data32_tx_gb;
wire [65:0] data66_rx_gb;
wire        data_next;
wire        data_valid;

reg         gearbox_en_rx;
reg         gearbox_en_tx;
wire        gearbox_rdy_rx;
wire        gearbox_rdy_tx;

// Scrambler Signals
reg [63:0]  data_in;
reg [1:0]   sync;
wire [65:0] data66_tx_scr;
wire        enable0;

// OSERDES Signals
reg rst_serdes;
wire [3:0] data_out_p;
wire [3:0] data_out_n;
reg [7:0]  piso0, piso1, piso2, piso3;
//wire [7:0] piso0_rev, piso1_rev, piso2_rev, piso3_rev;

// ISERDES Signals
wire [31:0] data32_iserdes;
wire [7:0]  sipo0, sipo1, sipo2, sipo3;

// Descrambler Signals
wire [63:0] data64_rx_uns;
wire [1:0]  sync_out;
reg         enable1;

// Block Sync
wire        blocksync_out;
wire        rxgearboxslip_out;
reg         gearbox_rdy_rx_r0;
reg         gearbox_rdy_rx_r1;

// Miscellaneous
reg         cnt;
reg         clkcnt = 0;
reg         shift0;
reg         shift1;

integer data = 0;

// Aurora Tx
scrambler scr (
    .data_in(data_in),
    .sync_info(sync),
    .enable(enable0&gearbox_rdy_tx),
    .clk(clk160),
    .rst(rst),
    .data_out(data66_tx_scr)
);

gearbox66to32 tx_gb (
    .rst(rst),
    .clk(clk160),
    .data66(data66_tx_scr),
    .gearbox_en(gearbox_en_tx),
    .gearbox_rdy(gearbox_rdy_tx),
    .data32(data32_tx_gb),
    .data_next(data_next)
);

//OSERDES Interface
cmd_oserdes piso0_1280(
   //.data_out_from_device(piso0_rev),
   .data_out_from_device(piso0),
   .data_out_to_pins_p(data_out_p[0]),
   .data_out_to_pins_n(data_out_n[0]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

cmd_oserdes piso1_1280(
   //.data_out_from_device(piso1_rev),
   .data_out_from_device(piso1),
   .data_out_to_pins_p(data_out_p[1]),
   .data_out_to_pins_n(data_out_n[1]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

cmd_oserdes piso2_1280(
   //.data_out_from_device(piso2_rev),
   .data_out_from_device(piso2),
   .data_out_to_pins_p(data_out_p[2]),
   .data_out_to_pins_n(data_out_n[2]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

cmd_oserdes piso3_1280(
   //.data_out_from_device(piso3_rev),
   .data_out_from_device(piso3),
   .data_out_to_pins_p(data_out_p[3]),
   .data_out_to_pins_n(data_out_n[3]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

//ISERDES Interface
cmd_iserdes i0 (
    .data_in_from_pins_p(data_out_p[0]),
    .data_in_from_pins_n(data_out_n[0]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    //.bitslip(rxgearboxslip_out),
    .bitslip(1'b0),
    .data_in_to_device(sipo0)
);

cmd_iserdes i1 (
    .data_in_from_pins_p(data_out_p[1]),
    .data_in_from_pins_n(data_out_n[1]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    //.bitslip(rxgearboxslip_out),
    .bitslip(1'b0),
    .data_in_to_device(sipo1)
);

cmd_iserdes i2 (
    .data_in_from_pins_p(data_out_p[2]),
    .data_in_from_pins_n(data_out_n[2]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    //.bitslip(rxgearboxslip_out),
    .bitslip(1'b0),
    .data_in_to_device(sipo2)
);

cmd_iserdes i3 (
    .data_in_from_pins_p(data_out_p[3]),
    .data_in_from_pins_n(data_out_n[3]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    //.bitslip(rxgearboxslip_out),
    .bitslip(1'b0),
    .data_in_to_device(sipo3)
);

// Aurora Rx
gearbox32to66 rx_gb (
    .rst(rst),
    .clk(clk160),
    .data32(data32_iserdes),
    .gearbox_en(gearbox_en_rx),
    .gearbox_rdy(gearbox_rdy_rx),
    .data66(data66_rx_gb),
    .data_valid(data_valid)
);

descrambler uns (
    .data_in(data66_rx_gb), 
    .sync_info(sync_out),
    .enable(!shift1&gearbox_rdy_rx),
    .clk(clk160),
    .rst(rst),
    .data_out(data64_rx_uns)
);

block_sync #
(
    .SH_CNT_MAX(16'd64),
    .SH_INVALID_CNT_MAX(10'd16)
)
b_sync
(
    .clk(clk160),
    .system_reset(rst),
    .blocksync_out(blocksync_out),
    .rxgearboxslip_out(rxgearboxslip_out),
    .rxheader_in(sync_out),
    .rxheadervalid_in(shift1&gearbox_rdy_rx)
);

initial begin
    enable1         <= 1'b0;
    rst             <= 1'b0;
    rst_serdes      <= 1'b0;
    clk160          <= 1'b1;
    clk640          <= 1'b1;
    cnt             <= 1'b0;
    gearbox_en_rx   <= 1'b0;
    gearbox_en_tx   <= 1'b0;

    $monitor("%d, %h", $time, data64_rx_uns);
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

always @(posedge clk160) begin
    cnt <= ~cnt;
end

always @(posedge clk160) begin
    shift0 <= data_valid;
    shift1 <= shift0;

    gearbox_rdy_rx_r0 <= gearbox_rdy_rx;
    gearbox_rdy_rx_r1 <= gearbox_rdy_rx_r0;
end

always @(posedge clk160) begin
    clkcnt <= clkcnt + 1;
    if (clkcnt == 1'b1) begin
        `ifdef PROPER_HEADERS
            // Proper headers
            sync <= ($urandom%2 == 1'b1) ? 2'b10 : 2'b01;
        `else
            // Improper headers
            sync <= $urandom%4;
        `endif
    end
end

// OSERDES 32 to 4x8
assign {piso0, piso1, piso2, piso3} = data32_tx_gb;

// assign piso0_rev = {piso0[0], piso0[1], piso0[2], piso0[3], piso0[4], piso0[5], piso0[6], piso0[7]};
// assign piso1_rev = {piso1[0], piso1[1], piso1[2], piso1[3], piso1[4], piso1[5], piso1[6], piso1[7]};
// assign piso2_rev = {piso2[0], piso2[1], piso2[2], piso2[3], piso2[4], piso2[5], piso2[6], piso2[7]};
// assign piso3_rev = {piso3[0], piso3[1], piso3[2], piso3[3], piso3[4], piso3[5], piso3[6], piso3[7]};


// ISERDES 4x8 to 32

// wire [7:0] sipo0_rev, sipo1_rev, sipo2_rev, sipo3_rev;
// assign sipo0_rev = {sipo0[0], sipo0[1], sipo0[2], sipo0[3], sipo0[4], sipo0[5], sipo0[6], sipo0[7]};
// assign sipo1_rev = {sipo1[0], sipo1[1], sipo1[2], sipo1[3], sipo1[4], sipo1[5], sipo1[6], sipo1[7]};
// assign sipo2_rev = {sipo2[0], sipo2[1], sipo2[2], sipo2[3], sipo2[4], sipo2[5], sipo2[6], sipo2[7]};
// assign sipo3_rev = {sipo3[0], sipo3[1], sipo3[2], sipo3[3], sipo3[4], sipo3[5], sipo3[6], sipo3[7]};
// 
// assign data32_iserdes = {sipo0_rev, sipo1_rev, sipo2_rev, sipo3_rev};

assign data32_iserdes = {sipo0, sipo1, sipo2, sipo3};

assign enable0 = ((cnt == 0)&&!rst);

initial begin
    @(posedge clk160);
    rst     <= 1'b1;
    rst_tx  <= 1'b1;
    rst_rx  <= 1'b1;
    data_in <= 1'b0;
    @(posedge clk160);
    rst_serdes <= 1'b1;
    @(posedge clk160);
    enable1 <= 1'b1;
    data_in <= {{8{4'hA}}, {8{4'hB}}};
    @(posedge clk160);
    rst     <= 1'b0;
    rst_serdes <= 1'b0;
    gearbox_en_tx <= 1'b1;
    @(posedge clk160);
    data_in <= 64'b0;
    @(posedge clk160);
    gearbox_en_rx <= 1'b1;
    @(posedge clk160);
    data_in <= {64{1'b1}};
    for (int i=0; i<200; i=i+1) begin
        repeat (2) @(posedge clk160);
        data_in <= {32'hc0ff_ee00, data};
        data <= data + 1;
    end
    data_in <= 64'hc0ff_ee00_c0ca_c01a;
    repeat (200) @(posedge clk160);
    repeat (25) @(posedge clk160);
    $stop;
end
    
endmodule
