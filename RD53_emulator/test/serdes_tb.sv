// Testbench: Gearbox + Scrambler + SERDES + Block Sync test
//
//           OSERDES              ISERDES           
//        +-----------+   8    +-----------+        
//   32   |           | --/--> |           |   32   
// --/--> | 32 to 4x8 | --/--> | 4x8 to 32 | --/--> 
//        |           | --/--> |           |        
//        +-----------+ --/--> +-----------+        
//

`timescale  1ns / 1ps

module serdes_tb();

// Clocks
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;

reg clk160;
reg clk640;

// Reset
reg rst_serdes;

// OSERDES Signals
reg [31:0] data32;
wire [3:0] data_out_p;
wire [3:0] data_out_n;
reg [7:0]  piso0, piso1, piso2, piso3;
//wire [7:0] piso0_rev, piso1_rev, piso2_rev, piso3_rev;

// ISERDES Signals
wire [31:0] data32_iserdes;
wire [7:0]  sipo0, sipo1, sipo2, sipo3;
reg rxgearboxslip_out;

integer data = 0;

//OSERDES Interface
cmd_oserdes piso0_1280(
   //.data_out_from_device(piso0_rev),
   .data_out_from_device(piso0),
   .data_out_to_pins_p(data_out_p[0]),
   .data_out_to_pins_n(data_out_n[0]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

cmd_oserdes piso1_1280(
   //.data_out_from_device(piso1_rev),
   .data_out_from_device(piso1),
   .data_out_to_pins_p(data_out_p[1]),
   .data_out_to_pins_n(data_out_n[1]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

cmd_oserdes piso2_1280(
   //.data_out_from_device(piso2_rev),
   .data_out_from_device(piso2),
   .data_out_to_pins_p(data_out_p[2]),
   .data_out_to_pins_n(data_out_n[2]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

cmd_oserdes piso3_1280(
   //.data_out_from_device(piso3_rev),
   .data_out_from_device(piso3),
   .data_out_to_pins_p(data_out_p[3]),
   .data_out_to_pins_n(data_out_n[3]),
   .clk_in(clk640),
   .clk_div_in(clk160),
   .io_reset(rst_serdes)
);

//ISERDES Interface
cmd_iserdes i0 (
    .data_in_from_pins_p(data_out_p[0]),
    .data_in_from_pins_n(data_out_n[0]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    .bitslip(rxgearboxslip_out),
    //.bitslip(1'b0),
    .data_in_to_device(sipo0)
);

cmd_iserdes i1 (
    .data_in_from_pins_p(data_out_p[1]),
    .data_in_from_pins_n(data_out_n[1]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    .bitslip(rxgearboxslip_out),
    //.bitslip(1'b0),
    .data_in_to_device(sipo1)
);

cmd_iserdes i2 (
    .data_in_from_pins_p(data_out_p[2]),
    .data_in_from_pins_n(data_out_n[2]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    .bitslip(rxgearboxslip_out),
    //.bitslip(1'b0),
    .data_in_to_device(sipo2)
);

cmd_iserdes i3 (
    .data_in_from_pins_p(data_out_p[3]),
    .data_in_from_pins_n(data_out_n[3]),
    .clk_in(clk640),
    .clk_div_in(clk160),
    .io_reset(rst_serdes),
    .bitslip(rxgearboxslip_out),
    //.bitslip(1'b0),
    .data_in_to_device(sipo3)
);

initial begin
    rst_serdes          <= 1'b0;
    clk160              <= 1'b1;
    clk640              <= 1'b1;
    data32              <= 32'h0000_0000;
    rxgearboxslip_out   <= 1'b0;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// OSERDES 32 to 4x8
assign {piso0, piso1, piso2, piso3} = data32;

// assign piso0_rev = {piso0[0], piso0[1], piso0[2], piso0[3], piso0[4], piso0[5], piso0[6], piso0[7]};
// assign piso1_rev = {piso1[0], piso1[1], piso1[2], piso1[3], piso1[4], piso1[5], piso1[6], piso1[7]};
// assign piso2_rev = {piso2[0], piso2[1], piso2[2], piso2[3], piso2[4], piso2[5], piso2[6], piso2[7]};
// assign piso3_rev = {piso3[0], piso3[1], piso3[2], piso3[3], piso3[4], piso3[5], piso3[6], piso3[7]};


// ISERDES 4x8 to 32

// wire [7:0] sipo0_rev, sipo1_rev, sipo2_rev, sipo3_rev;
// assign sipo0_rev = {sipo0[0], sipo0[1], sipo0[2], sipo0[3], sipo0[4], sipo0[5], sipo0[6], sipo0[7]};
// assign sipo1_rev = {sipo1[0], sipo1[1], sipo1[2], sipo1[3], sipo1[4], sipo1[5], sipo1[6], sipo1[7]};
// assign sipo2_rev = {sipo2[0], sipo2[1], sipo2[2], sipo2[3], sipo2[4], sipo2[5], sipo2[6], sipo2[7]};
// assign sipo3_rev = {sipo3[0], sipo3[1], sipo3[2], sipo3[3], sipo3[4], sipo3[5], sipo3[6], sipo3[7]};
// 
// assign data32_iserdes = {sipo0_rev, sipo1_rev, sipo2_rev, sipo3_rev};

assign data32_iserdes = {sipo0, sipo1, sipo2, sipo3};

initial begin
    @(posedge clk160);
    data32 <= 1'b0;
    @(posedge clk160);
    rst_serdes <= 1'b1;
    @(posedge clk160);
    data32 <= {8{4'hB}};
    @(posedge clk160);
    rst_serdes <= 1'b0;
    @(posedge clk160);
    data32 <= 32'b0;
    @(posedge clk160);
    @(posedge clk160);
    data32 <= {32{1'b1}};
    data <= {32'hc0ff_ee00};
    for (int i=0; i<200; i=i+1) begin
        @(posedge clk160);
        data32 <= data;
        data <= data + 1;
        // if (data == 4) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // if (data == 8) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // if (data == 12) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // if (data == 16) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // if (data == 20) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // if (data == 24) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // if (data == 28) begin
        //     rxgearboxslip_out <= 1'b1;
        // end
        // 
        // @(posedge clk160);
        // if (data == 5) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        // 
        // if (data == 9) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        // 
        // if (data == 13) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        // 
        // if (data == 17) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        // 
        // if (data == 21) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        // 
        // if (data == 25) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        // 
        // if (data == 29) begin
        //     rxgearboxslip_out <= 1'b0;
        // end
        
        
        if (data == 32'hc0ff_ee04) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        if (data == 32'hc0ff_ee08) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        if (data == 32'hc0ff_ee0c) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        if (data == 32'hc0ff_ee10) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        if (data == 32'hc0ff_ee14) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        if (data == 32'hc0ff_ee18) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        if (data == 32'hc0ff_ee1c) begin
            rxgearboxslip_out <= 1'b1;
        end
        
        
        @(posedge clk160);
        if (data == 32'hc0ff_ee05) begin
            rxgearboxslip_out <= 1'b0;
        end
        
        if (data == 32'hc0ff_ee09) begin
            rxgearboxslip_out <= 1'b0;
        end
        
        if (data == 32'hc0ff_ee0d) begin
            rxgearboxslip_out <= 1'b0;
        end
        
        if (data == 32'hc0ff_ee11) begin
            rxgearboxslip_out <= 1'b0;
        end
        
        if (data == 32'hc0ff_ee15) begin
            rxgearboxslip_out <= 1'b0;
        end
        
        if (data == 32'hc0ff_ee19) begin
            rxgearboxslip_out <= 1'b0;
        end
        
        if (data == 32'hc0ff_ee1d) begin
            rxgearboxslip_out <= 1'b0;
        end
        
    end
    data32 <= 32'hc0ca_c01a;
    repeat (200) @(posedge clk160);
    repeat (25) @(posedge clk160);
    $stop;
end
    
endmodule
