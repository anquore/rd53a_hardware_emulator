// Xilinx Aurora 64b/66b

module aurora_ip_tb();
 
 `timescale 1 ns / 1 ps
 
 module aurora_64b66b_0_TB;
 
 //*************************Parameter Declarations**************************
     parameter       SIM_MAX_TIME  = 99999999;//3000000000; //To quit the simulation
     parameter       DCLK_PERIOD   = 14.286; //70 MHz CLK
     parameter       CLOCKPERIOD_1 = 6.400	; 
     parameter       CLOCKPERIOD_2 = 6.400	; 

     parameter       INIT_CLOCKPERIOD_1 = 5.000	; 
     parameter       INIT_CLOCKPERIOD_2 = 5.000	; 
     parameter       DRP_CLOCKPERIOD_1  = 10.000	; 
     parameter       DRP_CLOCKPERIOD_2  = 10.000	; 

 //************************Internal Register Declarations*****************************
 
     //Freerunning Clock
       reg                reference_clk_1_n_r; 
       reg                reference_clk_2_n_r; 

       reg                init_clk_1_n_r; 
       reg                init_clk_2_n_r; 

       reg                drp_clk_1_r; 
       reg                drp_clk_2_r; 
       wire               drp_clk_1; 
       wire               drp_clk_2;          
 
     //Global signals
       reg                gsr_r; 
       reg                gts_r; 
       reg                reset_i; 
       reg                pma_init_r; 
       reg       gsr_done;            //Indicates the deassertion of GSR    
     
 
 //********************************Wire Declarations**********************************
     
     //Freerunning Clock         
       wire               reference_clk_1_p_r; 
       wire               reference_clk_2_p_r;          
       wire               init_clk_1_p_r; 
       wire               init_clk_2_p_r;          
 
 
 //Dut1
 
     //Error Detection Interface
       wire               hard_err_1_i;         
       wire               soft_err_1_i;         
 
     //Status 
       wire               channel_up_1_i;         
       wire               lane_up_1_i; 
 
 
     //GTX Serial I/O
       wire               rxp_1_i;  
       wire               rxn_1_i;  
     
       wire               txp_1_i;  
       wire               txn_1_i;  
 
     // Error signals from the frame checker
       wire    [0:7]      data_err_count_1_o;  
 
 //Dut2
 
     //Error Detection Interface
       wire               hard_err_2_i;         
       wire               soft_err_2_i;         
 
     //Status 
       wire               channel_up_2_i;         
       wire               lane_up_2_i; 
 
 
     //GTX Serial I/O
       wire               rxp_2_i;  
       wire               rxn_2_i;  
     
       wire               txp_2_i;  
       wire               txn_2_i;  
 
     // Error signals from the frame checker
       wire    [0:7]      data_err_count_2_o;  
 
       reg      channel_up1 = 1'b0;
       reg      channel_up2 = 1'b0;
       reg      flag        = 1'b0;
       reg      flag_1      = 1'b0;
       reg      flag_2      = 1'b1;
       reg      flag_3      = 1'b0;
       reg      flag_4      = 1'b1;
  
 //*********************************Main Body of Code**********************************
 
 
     //________________________ Tie offs ___________________________________
     
     assign  tied_to_ground_i     =    1'b0;
     
     //_________________________GTX Serial Connections________________
    
 
     assign   rxn_1_i      =    txn_2_i;
     assign   rxp_1_i      =    txp_2_i;
     assign   rxn_2_i      =    txn_1_i;
     assign   rxp_2_i      =    txp_1_i;
     
    
     //__________________________Global Signals_____________________________
     
     //Simultate the global reset that occurs after configuration at the beginning
     //of the simulation. Note that both GTX swift models use the same global signals.
     assign glbl.GSR = gsr_r;
     assign glbl.GTS = gts_r;
 
     initial
         begin
            gts_r      = 1'b0;        
            gsr_r      = 1'b1;
            gsr_done   = 1'b0;
            pma_init_r = 1'b1;
            reset_i = 1'b1;
            #(160*INIT_CLOCKPERIOD_1);
            gsr_r      = 1'b0;
            gsr_done   = 1'b1;
            pma_init_r = 1'b0;
            #60 reset_i = 1'b0;
     end
 
 
     //____________________________Clocks____________________________
 
     initial reference_clk_1_n_r = 1'b0;
 
 
     always #(CLOCKPERIOD_1 / 2) reference_clk_1_n_r = !reference_clk_1_n_r;
 
     assign reference_clk_1_p_r = !reference_clk_1_n_r;
 
 
 
     initial reference_clk_2_n_r = 1'b0;
 
 
     always #(CLOCKPERIOD_2 / 2) reference_clk_2_n_r = !reference_clk_2_n_r;
 
     assign reference_clk_2_p_r = !reference_clk_2_n_r;
 
 
     //--------------------------------------------------------------
 
     initial init_clk_1_n_r = 1'b0;
 
 
     always #(INIT_CLOCKPERIOD_1 / 2) init_clk_1_n_r = !init_clk_1_n_r;
 
     assign init_clk_1_p_r = !init_clk_1_n_r;
 
 
 
     initial init_clk_2_n_r = 1'b0;
 
 
     always #(INIT_CLOCKPERIOD_2 / 2) init_clk_2_n_r = !init_clk_2_n_r;
 
     assign init_clk_2_p_r = !init_clk_2_n_r;
 
 
     //--------------------------------------------------------------
     //____________________________DRP Clock_________________________
 
     initial drp_clk_1_r = 1'b0;
 
 
     always #(DRP_CLOCKPERIOD_1 / 2) drp_clk_1_r = !drp_clk_1_r;
 
     assign drp_clk_1 = !drp_clk_1_r;
 
 
 
     initial drp_clk_2_r = 1'b0;
 
 
     always #(DRP_CLOCKPERIOD_2 / 2) drp_clk_2_r = !drp_clk_2_r;
 
     assign drp_clk_2 = !drp_clk_2_r;
 
 
 
 
     //____________________________Resets____________________________
     
 
     //________________________Instantiate Dut 1 ________________
 
aurora_64b66b_0_exdes aurora_example_1_i
 (
     // User IO
     .RESET(reset_i),
     // Error signals from Aurora    
     .HARD_ERR			(hard_err_1_i),
     .SOFT_ERR			(soft_err_1_i),
 
     // Status Signals
     .LANE_UP(lane_up_1_i),
     .CHANNEL_UP		(channel_up_1_i),
 
 
     .INIT_CLK_P		(init_clk_1_p_r),
     .INIT_CLK_N		(init_clk_1_n_r),
     .PMA_INIT			(pma_init_r),
     .DRP_CLK_IN		(drp_clk_1),
 
     // Clock Signals
     .GTXQ2_P		(reference_clk_1_p_r),
     .GTXQ2_N		(reference_clk_1_n_r),
 
     // GT I/O
     .RXP			(rxp_1_i),
     .RXN			(rxn_1_i),
 
     .TXP			(txp_1_i),
     .TXN			(txn_1_i),
 
     // Error signals from the frame checker
     .DATA_ERR_COUNT		(data_err_count_1_o)
 );
 
     //________________________Instantiate Dut 2 ________________
 
aurora_64b66b_0_exdes aurora_example_2_i
 (
     // User IO
     .RESET(reset_i),
     // Error signals from Aurora    
     .HARD_ERR			(hard_err_2_i),
     .SOFT_ERR			(soft_err_2_i),
 
     // Status Signals
     .LANE_UP(lane_up_2_i),
     .CHANNEL_UP		(channel_up_2_i),
 
 
     .INIT_CLK_P		(init_clk_2_p_r),
     .INIT_CLK_N		(init_clk_2_n_r),
     .PMA_INIT			(pma_init_r),
     .DRP_CLK_IN		(drp_clk_2),
 
     // Clock Signals
     .GTXQ2_P		(reference_clk_2_p_r),
     .GTXQ2_N		(reference_clk_2_n_r),
 
     // GT I/O
     .RXP			(rxp_2_i),
     .RXN			(rxn_2_i),
 
     .TXP			(txp_2_i),
     .TXN			(txn_2_i),
 
     // Error signals from the frame checker
     .DATA_ERR_COUNT		(data_err_count_2_o)
 );
 
 
 
 
     always @ (posedge channel_up_1_i or posedge channel_up_2_i or
               posedge data_err_count_1_o or posedge data_err_count_2_o)
     begin
        if(channel_up_1_i == 1 && !channel_up1)
        begin
           $display("\n## aurora_64b66b_0_TB : @Time : %0t CHANNEL_UP is asserted in DUT1\n",$time);
           channel_up1 = 1'b1;
        end
  
        if(channel_up_2_i == 1 && !channel_up2)
        begin
           $display("\n## aurora_64b66b_0_TB : @Time : %0t CHANNEL_UP is asserted in DUT2\n",$time);
           channel_up2 = 1'b1;
        end
 
        if((data_err_count_1_o >  8'd0) || ( data_err_count_2_o >  8'd0))
        begin
           $display("\n## aurora_64b66b_0_TB : @Time : %0t ERROR : Test Fail\n",$time);
           #2000 $finish;
        end
           
        if(channel_up_1_i == 1 && channel_up_2_i == 1)
        begin
           #8000; 
           if(!((|data_err_count_1_o)  | (|data_err_count_2_o)))
           begin
              $display("\n## aurora_64b66b_0_TB : @Time : %0t Test Completed Successfully\n",$time);
              $finish;
           end
           else
           begin
              $display("\n## aurora_64b66b_0_TB : @Time : %0t Test Fail\n",$time);
              $finish;
           end
        end
     end
 
    //Abort the simulation when it reaches to max time limit
    initial
    begin
      #(SIM_MAX_TIME) $display("\n aurora_64b66b_0_TB : @Time : %0t ERROR : Reached max. simulation time limit\n",$time);
      $display("\n channel_up_2_i : %0h  channel_up_1_i : %0h   lane_up_2_i : %0h  lane_up_1_i :%0h ",channel_up_2_i,channel_up_1_i,lane_up_2_i,lane_up_1_i);
      $finish;
    end
 
 endmodule





endmodule