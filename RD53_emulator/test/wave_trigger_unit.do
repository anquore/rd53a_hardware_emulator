onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Inputs -color {Medium Sea Green} /trigger_unit_tb/t_unit/rst
add wave -noupdate -expand -group Inputs -color {Medium Sea Green} /trigger_unit_tb/t_unit/clk80
add wave -noupdate -expand -group Inputs -color {Medium Sea Green} /trigger_unit_tb/t_unit/clk160
add wave -noupdate -expand -group Inputs -color {Medium Sea Green} /trigger_unit_tb/t_unit/trigger
add wave -noupdate -expand -group Inputs -color {Medium Sea Green} /trigger_unit_tb/t_unit/trig_clr
add wave -noupdate -expand -group Outputs -color Coral /trigger_unit_tb/t_unit/trigger_rdy
add wave -noupdate -expand -group Outputs -color Coral /trigger_unit_tb/t_unit/enc_trig
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/trig_sr80
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/trig_sr160
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/trig_cnt
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/encoded_trig
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/encoded_trig_i
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/trig_load
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/trig_pres
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/encode_valid
add wave -noupdate -expand -group Internal -color Gold /trigger_unit_tb/t_unit/front_nback
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 161
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {989 ps}
