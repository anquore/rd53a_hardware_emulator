onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Shared Signals}
add wave -noupdate /rd53_txrx_tb/clk160
add wave -noupdate /rd53_txrx_tb/clk625
add wave -noupdate /rd53_txrx_tb/clk156p25
add wave -noupdate /rd53_txrx_tb/rst
add wave -noupdate -color {Medium Spring Green} -radix hexadecimal /rd53_txrx_tb/data_serial_p
add wave -noupdate -color {Medium Spring Green} -radix hexadecimal /rd53_txrx_tb/data_serial_n
add wave -noupdate -divider {Tx Signals}
add wave -noupdate -color Yellow -radix hexadecimal /rd53_txrx_tb/data_in
add wave -noupdate -color Yellow /rd53_txrx_tb/data_in_valid
add wave -noupdate -color Yellow /rd53_txrx_tb/service_frame
add wave -noupdate /rd53_txrx_tb/system_halt
add wave -noupdate /rd53_txrx_tb/full
add wave -noupdate /rd53_txrx_tb/empty
add wave -noupdate -radix hexadecimal /rd53_txrx_tb/tx_uut/data_post_scramble
add wave -noupdate -radix hexadecimal /rd53_txrx_tb/tx_uut/data32
add wave -noupdate -group piso -radix binary /rd53_txrx_tb/tx_uut/piso0
add wave -noupdate -group piso -radix binary /rd53_txrx_tb/tx_uut/piso1
add wave -noupdate -group piso -radix binary /rd53_txrx_tb/tx_uut/piso2
add wave -noupdate -group piso -radix binary /rd53_txrx_tb/tx_uut/piso3
add wave -noupdate -divider {Rx Signals}
add wave -noupdate -group sipo -radix binary /rd53_txrx_tb/rx_uut/sipo0
add wave -noupdate -group sipo -radix binary /rd53_txrx_tb/rx_uut/sipo1
add wave -noupdate -group sipo -radix binary /rd53_txrx_tb/rx_uut/sipo2
add wave -noupdate -group sipo -radix binary /rd53_txrx_tb/rx_uut/sipo3
add wave -noupdate -radix hexadecimal /rd53_txrx_tb/rx_uut/data32
add wave -noupdate -radix hexadecimal /rd53_txrx_tb/rx_uut/data66
add wave -noupdate /rd53_txrx_tb/rx_uut/gearbox_data_valid
add wave -noupdate /rd53_txrx_tb/rx_uut/sync_info
add wave -noupdate -color Magenta -radix hexadecimal /rd53_txrx_tb/data_out
add wave -noupdate -color Magenta /rd53_txrx_tb/data_out_valid
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {743896 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {937656 ps} {1003282 ps}
