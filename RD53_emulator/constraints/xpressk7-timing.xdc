create_clock -period 10.000 -name pcie_clk -waveform {0.000 5.000} [get_ports *pcie_clk*]
#create_clock -period 6.25 -waveform {0.000 3.125} [get_ports clk200_p]


#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT1]] 4.000
#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT1]] 4.000
#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT0]] 6.125
#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT0]] 6.125
#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT2]] 12.250
#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map0clk/inst/mmcm_adv_inst/CLKOUT2]] 12.250
#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/Map2clk/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT0]] 0.700
#set_max_delay -from [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/Map2clk/inst/mmcm_adv_inst/CLKOUT0]] 0.700

set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] 4.000
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT3]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] 4.000
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] 4.000

set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT3]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] 6.125
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] 6.125
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] 6.125

set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT3]] 12.250
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT3]] 12.250
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT3]] 12.250

set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT2]] 12.250
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT2]] 12.250
set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT3]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT2]] 12.250

#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT1]] 6.125
#set_max_delay -from [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT1]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] 6.125

#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT5]] -to [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT0]] 1.00
#set_max_delay -from [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT5]] 1.00

#set_max_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT1]] 6.125

#set_min_delay -from [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT5]] -to [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT0]] 0.1
#set_min_delay -from [get_clocks -of_objects [get_pins theTop/pll_fast/inst/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins theYarr/app_0/clk_gen_cmp/inst/mmcm_adv_inst/CLKOUT5]] 0.1

set_max_delay -to [get_pins theYarr/app_0/wb_exp_comp/cfg_interrupt_s_reg/CLR] 8.000
set_false_path -from [get_pins theYarr/app_0/wb_exp_comp/cfg_interrupt_s_reg/C] -to [get_pins theYarr/pcie_0/inst/inst/pcie_top_i/pcie_7x_i/pcie_block_i/CFGINTERRUPTN]


set_false_path -to [get_pins -hierarchical *pcie_id_s*D*]





#set_false_path -from [get_pins app_0/wb_exp_comp/cfg_interrupt_s_reg/C]
set_false_path -from [get_ports rst_n_i]

#create_clock -period 6.666666 -waveform {0.000 3.3333333} [get_ports user_clk_in_p]

#create_generated_clock -name ttc_decoder_i/rclk -source [get_pins on_chip_top_/pll_fast/clk_out1] -divide_by 3 [get_pins ttc_decoder_i/sample_reg/Q]
#create_generated_clock -name phase_sel_i/clk40_i -source [get_pins ttc_decoder_i/sample_reg/Q] -divide_by 4 [get_pins phase_sel_i/clk_out_reg/Q]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/inst/clk_out1]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/clk_out1]