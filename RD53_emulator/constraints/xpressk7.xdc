#Clocks
#SYSCLK
set_property IOSTANDARD LVDS [get_ports clk200_n]
set_property IOSTANDARD LVDS [get_ports clk200_p]
set_property PACKAGE_PIN AD12 [get_ports clk200_p]
set_property PACKAGE_PIN AD11 [get_ports clk200_n]
#PCIE_CLK_QO
set_property PACKAGE_PIN U8 [get_ports pcie_clk_p]
set_property PACKAGE_PIN U7 [get_ports pcie_clk_n]


#On-board switch reset
#GPIO_SW_C
set_property PACKAGE_PIN G12 [get_ports rst_n_i] 
set_property IOSTANDARD LVCMOS25 [get_ports rst_n_i]


#PCIe reset
#PCIE_PERST_LS
set_property IOSTANDARD LVCMOS25 [get_ports sys_rst_n_i]
set_property PACKAGE_PIN G25 [get_ports sys_rst_n_i]



#PCIe signals
set_property LOC GTXE2_CHANNEL_X0Y7 [get_cells {theYarr/pcie_0/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
#PCIE_RX0
set_property PACKAGE_PIN M5 [get_ports {pci_exp_rxn[0]}]
set_property PACKAGE_PIN M6 [get_ports {pci_exp_rxp[0]}]
set_property LOC GTXE2_CHANNEL_X0Y6 [get_cells {theYarr/pcie_0/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[1].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
#PCIE_RX1
set_property PACKAGE_PIN P5 [get_ports {pci_exp_rxn[1]}]
set_property PACKAGE_PIN P6 [get_ports {pci_exp_rxp[1]}]
set_property LOC GTXE2_CHANNEL_X0Y5 [get_cells {theYarr/pcie_0/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[2].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
#PCIE_RX2
set_property PACKAGE_PIN R3 [get_ports {pci_exp_rxn[2]}]
set_property PACKAGE_PIN R4 [get_ports {pci_exp_rxp[2]}]
set_property LOC GTXE2_CHANNEL_X0Y4 [get_cells {theYarr/pcie_0/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[3].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
#PCIE_RX3
set_property PACKAGE_PIN T5 [get_ports {pci_exp_rxn[3]}]
set_property PACKAGE_PIN T6 [get_ports {pci_exp_rxp[3]}]

# On-board LEDs
#GPIO_LED_#_LS
set_property PACKAGE_PIN AB8 [get_ports {usr_led_o[0]}]
set_property PACKAGE_PIN AA8 [get_ports {usr_led_o[1]}]
set_property PACKAGE_PIN AC9 [get_ports {usr_led_o[2]}]
#set_property PACKAGE_PIN AB9 [get_ports {usr_led_o[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {usr_led_o[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {usr_led_o[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {usr_led_o[2]}]

# On-board Switches
#GPIO_SW
set_property PACKAGE_PIN AG5 [get_ports {usr_sw_i[0]}]
set_property PACKAGE_PIN AA12 [get_ports {usr_sw_i[1]}]
set_property PACKAGE_PIN AB12 [get_ports {usr_sw_i[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {usr_sw_i[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {usr_sw_i[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {usr_sw_i[2]}]





#HPC FMC YARR
#la33
set_property PACKAGE_PIN H22 [get_ports user_cmd_out_n[0]]
set_property IOSTANDARD LVDS_25 [get_ports user_cmd_out_n[0]]
set_property PACKAGE_PIN H21 [get_ports user_cmd_out_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports user_cmd_out_p[0]]


#CMD
#la27
set_property PACKAGE_PIN D26 [get_ports user_clk_out_p]
set_property IOSTANDARD LVDS_25 [get_ports user_clk_out_p]
set_property PACKAGE_PIN C26 [get_ports user_clk_out_n]
set_property IOSTANDARD LVDS_25 [get_ports user_clk_out_n]


#USER_SMA_CLOCK_P/J11
set_property PACKAGE_PIN G22    [get_ports {fe_data_p_i[0][0]} ]
set_property PACKAGE_PIN F22    [get_ports {fe_data_n_i[0][0]} ]

set_property PACKAGE_PIN D21    [get_ports {fe_data_p_i[0][1]} ]
set_property PACKAGE_PIN C21    [get_ports {fe_data_n_i[0][1]} ]

set_property PACKAGE_PIN D22    [get_ports {fe_data_p_i[0][2]} ]
set_property PACKAGE_PIN C22    [get_ports {fe_data_n_i[0][2]} ]

set_property PACKAGE_PIN D16    [get_ports {fe_data_p_i[0][3]} ]
set_property PACKAGE_PIN C16    [get_ports {fe_data_n_i[0][3]} ]






#la00
set_property PACKAGE_PIN C25 [get_ports {eudet_rst_p}]
set_property PACKAGE_PIN B25 [get_ports {eudet_rst_n}]
#la02
set_property PACKAGE_PIN H24 [get_ports {eudet_clk_p}]
set_property PACKAGE_PIN H25 [get_ports {eudet_clk_n}]
#la8
set_property PACKAGE_PIN E29 [get_ports {eudet_busy_p}]
set_property PACKAGE_PIN E30 [get_ports {eudet_busy_n}]

#la3
set_property PACKAGE_PIN H26 [get_ports {eudet_trig_p}]
set_property PACKAGE_PIN H27 [get_ports {eudet_trig_n}]


#la21
set_property PACKAGE_PIN A20    [get_ports ext_trig_i_p[0]]
set_property PACKAGE_PIN A21    [get_ports ext_trig_i_n[0]]

#la29
set_property PACKAGE_PIN C17 [get_ports ext_trig_i_p[1]]
set_property PACKAGE_PIN B17 [get_ports ext_trig_i_n[1]]

#la25
set_property PACKAGE_PIN G17 [get_ports ext_trig_i_p[2]]
set_property PACKAGE_PIN F17 [get_ports ext_trig_i_n[2]]

#la22
set_property PACKAGE_PIN C20 [get_ports ext_trig_i_p[3]]
set_property PACKAGE_PIN B20 [get_ports ext_trig_i_n[3]]


#RD53A, LPC FMA

#la01_n
set_property PACKAGE_PIN AE23    [get_ports user_cmd_in_p[0]]
set_property IOSTANDARD LVDS_25 [get_ports user_cmd_in_p[0]]
set_property PACKAGE_PIN AF23    [get_ports user_cmd_in_n[0]]
set_property IOSTANDARD LVDS_25 [get_ports user_cmd_in_n[0]]

#la30
#set_property PACKAGE_PIN AB29    [get_ports user_cmd_in_p[0]]
#set_property IOSTANDARD LVDS_25 [get_ports user_cmd_in_p[0]]
#set_property PACKAGE_PIN AB30    [get_ports user_cmd_in_n[0]]
#set_property IOSTANDARD LVDS_25 [get_ports user_cmd_in_n[0]]

#la01_cc
set_property PACKAGE_PIN K25 [get_ports user_clk_in_n]
set_property IOSTANDARD LVDS_25 [get_ports user_clk_in_n]
#set_property DIFF_TERM TRUE [get_ports user_clk_in_n]
set_property PACKAGE_PIN L25 [get_ports user_clk_in_p]
set_property IOSTANDARD LVDS_25 [get_ports user_clk_in_p]
#set_property DIFF_TERM TRUE [get_ports user_clk_in_p]



set_property PACKAGE_PIN Y24 [get_ports clk_160_n_o]
set_property IOSTANDARD LVDS_25 [get_ports clk_160_n_o]
set_property DIFF_TERM TRUE [get_ports clk_160_n_o]
set_property PACKAGE_PIN Y23 [get_ports clk_160_p_o]
set_property IOSTANDARD LVDS_25 [get_ports clk_160_p_o]
set_property DIFF_TERM TRUE [get_ports clk_160_p_o]


#la31_p
#la31_n
set_property PACKAGE_PIN AD29 [get_ports fe_data_p_o[0][0]]
set_property PACKAGE_PIN AE29 [get_ports fe_data_n_o[0][0]]
#la29_cc_p
#la29_cc_n
set_property PACKAGE_PIN AE28 [get_ports fe_data_p_o[0][1]]
set_property PACKAGE_PIN AF28 [get_ports fe_data_n_o[0][1]]
#la32_p
#la32_n
set_property PACKAGE_PIN Y30 [get_ports fe_data_p_o[0][2]]
set_property PACKAGE_PIN AA30 [get_ports fe_data_n_o[0][2]]

#la33_p
#la33_n
set_property PACKAGE_PIN AC29 [get_ports fe_data_p_o[0][3]]
set_property PACKAGE_PIN AC30 [get_ports fe_data_n_o[0][3]]


## FMC_LPC_LA02
#set_property PACKAGE_PIN AF20   [get_ports fe_data_p_o[0][0]]
#set_property PACKAGE_PIN AF21   [get_ports fe_data_n_o[0][0]]

## Lane 1
## FMC_LPC_LA00
#set_property PACKAGE_PIN AD23   [get_ports fe_data_p_o[0][1]]
#set_property PACKAGE_PIN AE24   [get_ports fe_data_n_o[0][1]]

## Lane 2
## FMC_LPC_LA03
#set_property PACKAGE_PIN AG20   [get_ports fe_data_p_o[0][2]]
#set_property PACKAGE_PIN AH20   [get_ports fe_data_n_o[0][2]]

## Lane 3
## FMC_LPC_LA04
#set_property PACKAGE_PIN AH21   [get_ports fe_data_p_o[0][3]]
#set_property PACKAGE_PIN AJ21   [get_ports fe_data_n_o[0][3]]

#la13_p
#la13_n
set_property PACKAGE_PIN A25 [get_ports {sda_o}]
set_property PACKAGE_PIN A26 [get_ports {scl_o}]

#la05_p
#la05_n
set_property PACKAGE_PIN G29 [get_ports {latch_o}]
set_property PACKAGE_PIN F30 [get_ports {sdi_i}]

set_property IOSTANDARD LVCMOS25 [get_ports {scl_o}]
set_property IOSTANDARD LVCMOS25 [get_ports {sda_o}]
set_property IOSTANDARD LVCMOS25 [get_ports {latch_o}]
set_property IOSTANDARD LVCMOS25 [get_ports {sdi_i}]


set_property IOSTANDARD LVDS_25 [get_ports fe_data_*]
set_property DIFF_TERM TRUE [get_ports fe_data_*]
set_property DIFF_TERM TRUE [get_ports user_cmd*]
set_property IBUF_LOW_PWR FALSE [get_ports fe_data_p_i*]
set_property IBUF_LOW_PWR FALSE [get_ports fe_data_n_i*]
#set_property IBUF_LOW_PWR FALSE [get_ports fe_data_p_i*]
#set_property IBUF_LOW_PWR FALSE [get_ports fe_data_n_i*]

set_property IOSTANDARD LVDS_25 [get_ports ext_trig_*]
set_property DIFF_TERM TRUE [get_ports ext_trig_*]

set_property IOSTANDARD LVDS_25 [get_ports eudet_*]

