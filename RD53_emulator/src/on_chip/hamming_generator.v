// big endian hamming parity bit generator, six bit

// testing hamming.do

module hamming_generator(
    input [49:0] seed,
    output reg [5:0] parity_bits
);

reg [55:0] cat;  // concatenate
reg [55:0] to_XOR;

integer i, j;
   
always @(*) begin
    // leave open spots for the parity positions
    cat = {seed[49:26], 1'b0, seed[25:11], 1'b0, seed[10:4], 
           1'b0, seed[3:1], 1'b0, seed[0], 2'b0};

    for (i = 0; i < 6; i = i + 1) begin
        for (j = 1; j < 57; j = j + 1) begin
            // if index is under current parity bit coverage,
            // pass its value to to_XOR
            to_XOR[j-32'b1] = j[i] & cat[j-32'b1];
        end
        // if it's a parity location, set it to zero;
        to_XOR[1:0] = 2'b0; to_XOR[3]  = 1'b0;
        to_XOR[7]   = 1'b0; to_XOR[15] = 1'b0;
        to_XOR[31]  = 1'b0;
       
        parity_bits[i] = ^to_XOR;
    end
end

endmodule
