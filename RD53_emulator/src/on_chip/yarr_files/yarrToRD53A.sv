module yarrToRD53Aemu(
   //clks, resets, and user interaction
   input pcie_clk_p,
   input pcie_clk_n,
   input clk200_n,
   input clk200_p,
   input rst_n_i,
   input sys_rst_n_i,
   //input ilaActivate,
   input [2:0] usr_sw_i,
   output [2:0] usr_led_o,
   
   //pcie
   output [3:0] pci_exp_txn,
   output [3:0] pci_exp_txp,
   input [3:0] pci_exp_rxn,
   input [3:0] pci_exp_rxp,
   
   //clks, cmd, and data yarr
   output user_clk_out_p,
   output user_clk_out_n,
   
   input [3:0] fe_data_p_i [0:0],
   input [3:0] fe_data_n_i [0:0],
   
   
   input user_clk_in_p,
   input user_clk_in_n,
   
   //clks,. cmd, and data RD53
   output [0:0] user_cmd_out_p,
   output [0:0] user_cmd_out_n,
   
   
   input user_cmd_in_p[0:0],
   input user_cmd_in_n[0:0],
   
   output [3:0] fe_data_p_o [0:0],
   output [3:0] fe_data_n_o [0:0],
   
   input [3:0] ext_trig_i_p,
   input [3:0] ext_trig_i_n,
   
   input eudet_trig_p,
   input eudet_trig_n,
   
   output eudet_busy_p,
   output eudet_busy_n,
   
   input eudet_rst_p,
   input eudet_rst_n,
   
   output eudet_clk_p,
   output eudet_clk_n,
   
   output scl_o, 
   output sda_o, 
   input sdi_i, 
   output latch_o,
   output clk_160_p_o,
   output clk_160_n_o

);

  //the emulator
  logic [7:0] RD53Aleds;
  logic [3:0] RD53Adebug;
  logic [3:0] OFB;
  //logic trig_out;
  
  //YARR connections
  //logic fe_clk_p, fe_clk_n;
  //logic fe_cmd_p [0:0];
  //logic fe_cmd_n[0:0];
  logic trig_out [0:0];
  //logic fe_cmd [0:0];
  
  logic [3:0][3:0] fe_data [0:0];
  logic ila_ttcData, ila_gotClk, ila_sentClk, ila_sentCmd, ila_data_read_o;
  logic [31:0] ila_cmdFull;
  
  logic sentCmdDifferent [0:0];
  assign sentCmdDifferent[0] = ila_sentCmd;
  
  logic clk_160_n, clk_640_n, clk_80_n, clk_40_n;
  logic [3:0][7:0] data8;
  logic [3:0] bitslip;
  logic [31:0] data8_re;
  assign data8_re[7:0] = data8[0];
  assign data8_re[15:8] = data8[1];
  assign data8_re[23:16] = data8[2];
  assign data8_re[31:24] = data8[3];
  
  on_chip_top theTop
  (.rst(rst_n_i)
  //,.testClk(ila_sentClk)
 /* ,.clk_640_i(clk_640_n)
     ,.clk_40_i(clk_40_n)
      ,.clk_80_i(clk_80_n)
       ,.clk_160_i(clk_160_n)*/
  ,.USER_SMA_CLOCK_P(user_clk_in_p)
  ,.USER_SMA_CLOCK_N(user_clk_in_n)
  ,.ttc_data_p(user_cmd_in_p)
  ,.ttc_data_n(user_cmd_in_n)
  //,.ttc_data(sentCmdDifferent)
  ,.cmd_out_p(fe_data_p_o)
  ,.cmd_out_n(fe_data_n_o)
  //,.cmd_out(fe_data)
  ,.trig_out(trig_out)
  ,.led(RD53Aleds)
  ,.debug(RD53Adebug)
  /*,.ila_cmdFull(ila_cmdFull)
  ,.ila_data_read_o(ila_data_read_o)
  ,.ilaActivate(usr_sw_i[0])
  ,.OFB_o(OFB)
  ,.data8_o(data8_re)
  ,.bitslip_i(bitslip)
  ,.button1(usr_sw_i[1])*/);
  
  /*logic [3:0] fe_data_n, fe_data_p;
  assign fe_data_p = fe_data[0];
  
  assign fe_data_n[0] = ~fe_data[0][0];
  assign fe_data_n[1] = ~fe_data[1][0];
  assign fe_data_n[2] = ~fe_data[2][0];
  assign fe_data_n[3] = ~fe_data[3][0];*/
  
  //the YARR
  logic ila_clk_o;
  assign sda_o = sda_s;
  //assign sdi_i = 1'b1;
  logic [0:0] fe_cmd_yarr;
  logic sda_s;
  top_level theYarr
  (.pcie_clk_p
  ,.pcie_clk_n
  ,.clk200_n
  ,.clk200_p
  ,.rst_n_i
  ,.sys_rst_n_i
  ,.pci_exp_txn           
  ,.pci_exp_txp         
  ,.pci_exp_rxn           
  ,.pci_exp_rxp
  ,.usr_sw_i
  ,.usr_led_o
  ,.fe_clk_p(user_clk_out_p)
  ,.fe_clk_n(user_clk_out_n)
  ,.fe_cmd_p(user_cmd_out_p)
  ,.fe_cmd_n(user_cmd_out_n)
  //,.fe_cmd(fe_cmd_yarr)
  ,.fe_data_p(fe_data_p_i[0])
  ,.fe_data_n(fe_data_n_i[0])
  ,.scl_o
  ,.sda_o(sda_s)
  ,.sdi_i
  ,.latch_o
  ,.ext_trig_i_p
  ,.ext_trig_i_n
  ,.eudet_trig_p
  ,.eudet_trig_n
  ,.eudet_busy_p
  ,.eudet_busy_n
  ,.eudet_rst_p
  ,.eudet_rst_n
  ,.eudet_clk_p
  ,.eudet_clk_n
  //,.ila_clk_o
  //,.ila_sentClk
  ,.ila_sentCmd
  ,.clk_640_o(clk_640_n)
  ,.clk_40_o(clk_40_n)
  ,.clk_80_o(clk_80_n)
  ,.clk_160_o(clk_160_n)
  ,.ila_cmdFull
  ,.ila_data_read_o
  ,.ilaActivate(usr_sw_i[0])
  ,.OFB_i(OFB)
  ,.bitslip(bitslip)
  ,.data8_i(data8)
  ,.clk_160_p_o(clk_160_p_o)
  ,.clk_160_n_o(clk_160_n_o));
  
  //ila
    

          
          
    /*OBUFDS OBUFDS_0(
    .I(fe_data[0][0]),
    .O(fe_data_p_o[0][0]),
    .OB(fe_data_n_o[0][0])
    );
    OBUFDS OBUFDS_1(
    .I(fe_data[0][1]),
    .O(fe_data_p_o[0][1]),
    .OB(fe_data_n_o[0][1])
    );
    OBUFDS OBUFDS_2(
    .I(fe_data[0][2]),
    .O(fe_data_p_o[0][2]),
    .OB(fe_data_n_o[0][2])
    );
    OBUFDS OBUFDS_3(
    .I(fe_data[0][3]),
    .O(fe_data_p_o[0][3]),
    .OB(fe_data_n_o[0][3])
    );*/
                   
  
  
  
  
  
  
  
  
  /*
  IBUFDS IBUFDS1_i(
                   .O(outClk),
                   .I(user_clk_out_p),
                   .IB(user_clk_out_n)
                );    */  
  
  //assign fe_cmd[0] = fe_cmd_yarr[0];
  
  //assign user_cmd_out_p = fe_cmd_p[0];
  //assign user_cmd_out_n = fe_cmd_n[0];
endmodule