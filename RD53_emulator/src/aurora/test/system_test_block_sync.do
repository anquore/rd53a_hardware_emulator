vlib work

vlog -sv -work work ../gearbox_66_to_32.sv
vlog -work work ../gearbox_32_to_66.v

# Using RD53A Scrambler and testing against custom Descrmabler
#vlog -work work ../scrambler_64b_58_39_1.sv

vlog -work work ../scrambler.v
vlog -work work ../descrambler.v

vlog -work work ../block_sync.v

vlog -work work ./system_test_block_sync_tb.sv

vsim -t 1ps -novopt system_test_block_sync_tb

view signals
view wave

do wave_system_test_block_sync.do

run -all