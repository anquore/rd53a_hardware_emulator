vlib work

vcom -work work ../fifo_fwft_funcsim.vhdl

vlog -work work ./fifo_test_tb_fwft.sv

vsim -t 1fs -novopt fifo_test_tb_fwft

view signals
view wave

do wave_fifo_test_fwft.do

run -all
