onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Tan /system_test_multi_serdes_tb/clk40
add wave -noupdate -color Tan /system_test_multi_serdes_tb/clk160
add wave -noupdate -color Tan /system_test_multi_serdes_tb/clk640
add wave -noupdate -color Tan /system_test_multi_serdes_tb/clk1280
add wave -noupdate /system_test_multi_serdes_tb/shift_n
add wave -noupdate /system_test_multi_serdes_tb/shift_p
add wave -noupdate -color Coral -expand -subitemconfig {{/system_test_multi_serdes_tb/rst_tx[3]} {-color Coral -height 15} {/system_test_multi_serdes_tb/rst_tx[2]} {-color Coral -height 15} {/system_test_multi_serdes_tb/rst_tx[1]} {-color Coral -height 15} {/system_test_multi_serdes_tb/rst_tx[0]} {-color Coral -height 15}} /system_test_multi_serdes_tb/rst_tx
add wave -noupdate -color Gold -expand -subitemconfig {{/system_test_multi_serdes_tb/rst_rx[3]} {-color Gold -height 15} {/system_test_multi_serdes_tb/rst_rx[2]} {-color Gold -height 15} {/system_test_multi_serdes_tb/rst_rx[1]} {-color Gold -height 15} {/system_test_multi_serdes_tb/rst_rx[0]} {-color Gold -height 15}} /system_test_multi_serdes_tb/rst_rx
add wave -noupdate -color {Medium Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/data_in[0]} {-color {Medium Slate Blue} -height 15} {/system_test_multi_serdes_tb/data_in[1]} {-color {Medium Slate Blue} -height 15} {/system_test_multi_serdes_tb/data_in[2]} {-color {Medium Slate Blue} -height 15} {/system_test_multi_serdes_tb/data_in[3]} {-color {Medium Slate Blue} -height 15}} /system_test_multi_serdes_tb/data_in
add wave -noupdate -color {Medium Aquamarine} -expand -subitemconfig {{/system_test_multi_serdes_tb/data_out[0]} {-color {Medium Aquamarine} -height 15} {/system_test_multi_serdes_tb/data_out[1]} {-color {Medium Aquamarine} -height 15} {/system_test_multi_serdes_tb/data_out[2]} {-color {Medium Aquamarine} -height 15} {/system_test_multi_serdes_tb/data_out[3]} {-color {Medium Aquamarine} -height 15}} /system_test_multi_serdes_tb/data_out
add wave -noupdate -expand /system_test_multi_serdes_tb/sync
add wave -noupdate /system_test_multi_serdes_tb/gearbox_rdy_tx
add wave -noupdate /system_test_multi_serdes_tb/sync_out
add wave -noupdate /system_test_multi_serdes_tb/channel_bonded
add wave -noupdate -expand /system_test_multi_serdes_tb/blocksync_out
add wave -noupdate /system_test_multi_serdes_tb/gearbox_rdy_rx
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/rst}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/clk40}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/clk160}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/clk640}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/data_in}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/sync}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/gearbox_rdy}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/data_out_p}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/data_out_n}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/data32_gb_tx}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/data_next}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/data66_tx_scr}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/piso}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/tx_buffer}
add wave -noupdate -expand -group {Lane 0} -expand -group {Lane 0 Tx} -color Salmon {/system_test_multi_serdes_tb/tx_core[0]/tx_lane/tx_buf_cnt}
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/clk40
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/rst
add wave -noupdate -expand -group {Channel Bonding} /system_test_multi_serdes_tb/cb/fifo_rst
add wave -noupdate -expand -group {Channel Bonding} /system_test_multi_serdes_tb/cb/fifo_rst_cnt
add wave -noupdate -expand -group {Channel Bonding} /system_test_multi_serdes_tb/cb/init_cnt
add wave -noupdate -expand -group {Channel Bonding} /system_test_multi_serdes_tb/cb/wr_en_rst
add wave -noupdate -expand -group {Channel Bonding} -color {Lime Green} /system_test_multi_serdes_tb/cb/channel_bonded
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/blocksync_out[3]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/blocksync_out[2]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/blocksync_out[1]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/blocksync_out[0]} {-color {Slate Blue} -height 15}} /system_test_multi_serdes_tb/cb/blocksync_out
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/data_valid_cb
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/data_out_cb[0]} {-color Coral -height 15} {/system_test_multi_serdes_tb/cb/data_out_cb[1]} {-color Coral -height 15} {/system_test_multi_serdes_tb/cb/data_out_cb[2]} {-color Coral -height 15} {/system_test_multi_serdes_tb/cb/data_out_cb[3]} {-color Coral -height 15}} /system_test_multi_serdes_tb/cb/data_out_cb
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/sync_out_cb[0]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/sync_out_cb[1]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/sync_out_cb[2]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/sync_out_cb[3]} {-color {Slate Blue} -height 15}} /system_test_multi_serdes_tb/cb/sync_out_cb
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/cb_char
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/cb_detect[3]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/cb_detect[2]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/cb_detect[1]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/cb_detect[0]} {-color {Slate Blue} -height 15}} /system_test_multi_serdes_tb/cb/cb_detect
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/data_in[0]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/data_in[1]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/data_in[2]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/data_in[3]} {-color {Slate Blue} -height 15}} /system_test_multi_serdes_tb/cb/data_in
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/data_valid
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/empty
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/full
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} /system_test_multi_serdes_tb/cb/gearbox_rdy_rx
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/rd_en[3]} {-color Aquamarine -height 15} {/system_test_multi_serdes_tb/cb/rd_en[2]} {-color Aquamarine -height 15} {/system_test_multi_serdes_tb/cb/rd_en[1]} {-color Aquamarine -height 15} {/system_test_multi_serdes_tb/cb/rd_en[0]} {-color Aquamarine -height 15}} /system_test_multi_serdes_tb/cb/rd_en
add wave -noupdate -expand -group {Channel Bonding} -color {Slate Blue} -expand -subitemconfig {{/system_test_multi_serdes_tb/cb/sync_in[0]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/sync_in[1]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/sync_in[2]} {-color {Slate Blue} -height 15} {/system_test_multi_serdes_tb/cb/sync_in[3]} {-color {Slate Blue} -height 15}} /system_test_multi_serdes_tb/cb/sync_in
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4329324884 fs} 0} {{Cursor 2} {216228897638 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 202
configure wave -valuecolwidth 174
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {4221467284 fs} {4478532716 fs}
