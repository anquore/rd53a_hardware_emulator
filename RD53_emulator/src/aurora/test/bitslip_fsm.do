vlib work

vlog -work work ../block_sync.v
vlog -work work ../bitslip_fsm.sv
vlog -work work ./bitslip_fsm_tb.sv

vsim -t 1ns -novopt bitslip_fsm_tb

view signals
view wave

do wave_bitslip_fsm.do

run -all