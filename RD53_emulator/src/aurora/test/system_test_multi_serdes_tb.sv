// Engineer: Lev Kurilenko
// Date: 8/22/2017
// Email: levkur@uw.edu
//
// Testbench: Multilane - Gearbox + Scrambler + SERDES + Block Sync + Bitslip FSM + Channel Bonding
// Description: Testing an Aurora 64b/66b multi-lane (4 lanes) Simplex connection.
//              Particularly targeting and testing the Channel Bonding mechanism.
//
//
//  X4
//  |----------------------------------------------------------------------------------------------------------------------------------------------------------------|
//  |                                                                                                                                                                |
//  |                       Tx Gearbox            OSERDES              ISERDES            Rx Gearbox                                                                 |
//  |  +----------+        +----------+        +-----------+        +-----------+        +----------+        +----------+   2             2   +-------------+        |
//  |  |   Tx     |   66   |          |   32   |           |   8    |           |   32   |          |   66   |   Rx     | --/-->  sync  --/-->| Block Sync  |        |
//  |  |Scrambler | --/--> | 66 to 32 | --/--> |  32 to 8  | --/--> |  8 to 32  | --/--> | 32 to 66 | --/--> |   De     |   64                +-------------+        |
//  |  |          |        |          |        |           |        |           |        |          |        |Scrambler | --/-->  data               ||              |
//  |  +----------+        +----------+        +-----------+        +-----------+        +----------+        +----------+                       +----------+         |
//  |                                                                    |                    |                                                 |          |         |
//  |                                                                    |                    \-------------------- gearbox_slip ---------------|   Bit    |         |
//  |                                                                    |                                                                      |   Slip   |         |
//  |                                                                    \----------------------------------------- iserdes_slip ---------------|   FSM    |         |
//  |                                                                                                                                           +----------+         |
//  |                                                                                                                                                                |
//  |----------------------------------------------------------------------------------------------------------------------------------------------------------------|
//
//
//

`timescale  1ns / 1fs

`define PROPER_HEADERS

module system_test_multi_serdes_tb();

localparam cb_char = 64'h7800_0000_0000_0040;   // From Memory may need to be changed

// Reset
reg [3:0] rst_tx;
reg [3:0] rst_rx;

// Clocks
parameter half_clk40_period = 12.5;
parameter clk160_period = 6.25;
parameter clk640_period = 1.5625;
parameter clk1280_period = 0.78125;

reg clk40;
reg clk160;
reg clk640;
reg clk1280;

// Aurora Tx Signals
wire [3:0]  data_out_p;
wire [3:0]  data_out_n;
reg  [63:0] data_in[4];
reg  [63:0] data_in_incr[4];
reg  [1:0]  sync[4];
wire [3:0]  gearbox_rdy_tx;

// Aurora Rx Signals
wire [63:0] data_out[4];
wire [1:0]  sync_out[4];
wire [3:0]  blocksync_out;
wire [3:0]  gearbox_rdy_rx;
wire [3:0]  data_valid;

// Aurora Channel Bonding Signals
wire [63:0] data_out_cb[4];
wire [1:0]  sync_out_cb[4];
wire data_valid_cb;
wire channel_bonded;

// Output Files
integer     bonded_data_out;

//===================
// Clock Generation
//===================

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// 1.28 GHz clock
always #(clk1280_period/2) begin
    clk1280 <= ~clk1280;
end

// Init
initial begin
    rst_tx          <= 4'h0;
    rst_rx          <= 4'h0;
    clk40           <= 1'b1;
    clk160          <= 1'b1;
    clk640          <= 1'b1;
    clk1280         <= 1'b1;
    for (int i=0; i <4; i = i + 1) begin
        data_in[i] <= {32'hc0ff_ee00, 32'h0000_0000};
        data_in_incr[i] <= 64'h0000_0000_0000_0000;
        sync[i] <= 2'b10;
    end
    
    bonded_data_out = $fopen("./system_test_multi_serdes_files/output_data.txt","w");
    $fwrite(bonded_data_out, "channel_bonded, data_valid_cb, sync_out_cb, data_out_cb");
end

// Main Data Driver
initial begin
    @(posedge clk40);
    rst_tx <= 4'h0;
    rst_rx <= 4'h0;
    repeat (2) @(posedge clk40);
    rst_tx <= 4'hF;
    rst_rx <= 4'hF;
    repeat (4) @(posedge clk40);
    rst_tx <= 4'h0;
    @(posedge clk40);
    @(posedge clk40);
    for (int i=0; i <4; i = i + 1) begin
        data_in[i] <= {32'hc0ff_ee00, 32'h0000_0000};
        sync[i] <= 2'b10;
    end
    @(posedge clk40);
    repeat (2) @(posedge clk40);
    for (int i=0; i<20_000; i=i+1) begin
        repeat (2) @(posedge clk40);
        for (int j=0; j <4; j = j + 1) begin
            if (gearbox_rdy_tx[j]) begin
                if (i%64 == 0) begin
                    data_in[j] <= cb_char;
                    sync[j] <= 2'b10;
                end
                else begin
                    data_in_incr[j] <= data_in_incr[j] + 1;
                    data_in[j] <= data_in_incr[j];
                    sync[j] <= 2'b01;
                end
            end
        end
        
        // Set what data iteration to deassert Rx Reset
        if (i == 1500) begin
            rst_rx[0] <= 1'b0;
            rst_rx[1] <= 1'b0;
            rst_rx[2] <= 1'b0;
            rst_rx[3] <= 1'b0;
        end
    end
    for (int j=0; j <4; j = j + 1) begin
        data_in[j] <= 64'hc0ff_ee00_c0ca_c01a;
    end
    repeat (200) @(posedge clk40);
    repeat (25) @(posedge clk40);
    
    $fclose(bonded_data_out);
    $stop;
end

always @(posedge clk40) begin
    if (data_valid_cb&channel_bonded) begin
        if ((data_out_cb[0][65:0] != data_out_cb[1][65:0]) || 
            (data_out_cb[0][65:0] != data_out_cb[2][65:0]) || 
            (data_out_cb[0][65:0] != data_out_cb[3][65:0])) begin
                $fwrite(bonded_data_out, "%d, %b, %b, %h, %h\n", $time, channel_bonded, data_valid_cb, sync_out_cb[0], data_out_cb[0]);
                $fwrite(bonded_data_out, "%d, %b, %b, %h, %h\n", $time, channel_bonded, data_valid_cb, sync_out_cb[1], data_out_cb[1]);
                $fwrite(bonded_data_out, "%d, %b, %b, %h, %h\n", $time, channel_bonded, data_valid_cb, sync_out_cb[2], data_out_cb[2]);
                $fwrite(bonded_data_out, "%d, %b, %b, %h, %h\n", $time, channel_bonded, data_valid_cb, sync_out_cb[3], data_out_cb[3]);
        end
    end
end

//============================================================================
//                            Module Instantiation
//============================================================================

//===================
// Aurora Tx
//===================

genvar i;

generate
    for (i = 0 ; i <= 3 ; i = i+1)
        begin : tx_core
            aurora_tx_top tx_lane (
                .rst(rst_tx[i]),
                .clk40(clk40),
                .clk160(clk160),
                .clk640(clk640),
                .data_in(data_in[i]),
                .sync(sync[i]),
                .gearbox_rdy(gearbox_rdy_tx[i]),
                .data_out_p(data_out_p[i]),
                .data_out_n(data_out_n[i])
            );
    end
endgenerate

// aurora_tx_top tx_lane_0 (
//     .rst(rst_tx[0]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     .data_in(data_in[0]),
//     .sync(sync[0]),
//     .gearbox_rdy(gearbox_rdy_tx[0]),
//     .data_out_p(data_out_p[0]),
//     .data_out_n(data_out_n[0])
// );
// 
// aurora_tx_top tx_lane_1 (
//     .rst(rst_tx[1]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     .data_in(data_in[1]),
//     .sync(sync[1]),
//     .gearbox_rdy(gearbox_rdy_tx[1]),
//     .data_out_p(data_out_p[1]),
//     .data_out_n(data_out_n[1])
// );
// 
// aurora_tx_top tx_lane_2 (
//     .rst(rst_tx[2]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     .data_in(data_in[2]),
//     .sync(sync[2]),
//     .gearbox_rdy(gearbox_rdy_tx[2]),
//     .data_out_p(data_out_p[2]),
//     .data_out_n(data_out_n[2])
// );
// 
// aurora_tx_top tx_lane_3 (
//     .rst(rst_tx[3]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     .data_in(data_in[3]),
//     .sync(sync[3]),
//     .gearbox_rdy(gearbox_rdy_tx[3]),
//     .data_out_p(data_out_p[3]),
//     .data_out_n(data_out_n[3])
// );

//===================
// Aurora Rx
//===================

parameter shift_size = 64;

reg [shift_size-1:0] shift_p[4];
reg [shift_size-1:0] shift_n[4];

always @(posedge clk1280) begin
    for (int i=0; i<4; i=i+1) begin
        for (int j=0; j<shift_size; j=j+1) begin
            if (j == 0) begin
                shift_p[i][0] <= data_out_p[i];
            end
            else begin
                shift_p[i][j] <= shift_p[i][j-1];
            end
        end
        for (int j=0; j<shift_size; j=j+1) begin
            if (j == 0) begin
                shift_n[i][0] <= data_out_n[i];
            end
            else begin
                shift_n[i][j] <= shift_n[i][j-1];
            end
        end
    end
end

// aurora_rx_top rx_lane_0 (
//     .rst(rst_rx[0]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     //.data_in_p(data_out_p[0]),
//     //.data_in_n(data_out_n[0]),
//     .data_in_p(shift_p[0][20]),
//     .data_in_n(shift_n[0][20]),
//     .blocksync_out(blocksync_out[0]),
//     .gearbox_rdy(gearbox_rdy_rx[0]),
//     .data_valid(data_valid[0]),
//     .sync_out(sync_out[0]),
//     .data_out(data_out[0])
// );
// 
// aurora_rx_top rx_lane_1 (
//     .rst(rst_rx[1]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     //.data_in_p(data_out_p[1]),
//     //.data_in_n(data_out_n[1]),
//     .data_in_p(shift_p[1][25]),
//     .data_in_n(shift_n[1][25]),
//     .blocksync_out(blocksync_out[1]),
//     .gearbox_rdy(gearbox_rdy_rx[1]),
//     .data_valid(data_valid[1]),
//     .sync_out(sync_out[1]),
//     .data_out(data_out[1])
// );
// 
// aurora_rx_top rx_lane_2 (
//     .rst(rst_rx[2]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     //.data_in_p(data_out_p[2]),
//     //.data_in_n(data_out_n[2]),
//     .data_in_p(shift_p[2][61]),
//     .data_in_n(shift_n[2][61]),
//     .blocksync_out(blocksync_out[2]),
//     .gearbox_rdy(gearbox_rdy_rx[2]),
//     .data_valid(data_valid[2]),
//     .sync_out(sync_out[2]),
//     .data_out(data_out[2])
// );
// 
// aurora_rx_top rx_lane_3 (
//     .rst(rst_rx[3]),
//     .clk40(clk40),
//     .clk160(clk160),
//     .clk640(clk640),
//     //.data_in_p(data_out_p[3]),
//     //.data_in_n(data_out_n[3]),
//     .data_in_p(shift_p[3][45]),
//     .data_in_n(shift_n[3][45]),
//     .blocksync_out(blocksync_out[3]),
//     .gearbox_rdy(gearbox_rdy_rx[3]),
//     .data_valid(data_valid[3]),
//     .sync_out(sync_out[3]),
//     .data_out(data_out[3])
// );

//genvar j;

generate
    for (i = 0 ; i <= 3 ; i = i+1)
        begin : rx_core
            aurora_rx_top rx_lane (
                .rst(rst_rx[i]),
                .clk40(clk40),
                .clk160(clk160),
                .clk640(clk640),
                .data_in_p(data_out_p[i]),
                .data_in_n(data_out_n[i]),
                //.data_in_p(shift_p[3][45]),
                //.data_in_n(shift_n[3][45]),
                .blocksync_out(blocksync_out[i]),
                .gearbox_rdy(gearbox_rdy_rx[i]),
                .data_valid(data_valid[i]),
                .sync_out(sync_out[i]),
                .data_out(data_out[i])
            );
            
            // aurora_rx_top_xapp rx_lane (
            //     .rst(rst_rx[i]),
            //     .clk40(clk40),
            //     .clk160(clk160),
            //     .clk640(clk640),
            //     .data_in_p(data_out_p[i]),
            //     .data_in_n(data_out_n[i]),
            //     //.data_in_p(shift_p[3][45]),
            //     //.data_in_n(shift_n[3][45]),
            //     .blocksync_out(blocksync_out[i]),
            //     .gearbox_rdy(gearbox_rdy_rx[i]),
            //     .data_valid(data_valid[i]),
            //     .sync_out(sync_out[i]),
            //     .data_out(data_out[i])
            // );
    end
endgenerate

//======================================
//      Aurora Channel Bonding
//======================================
channel_bond cb (
    .rst(rst_rx[0]),
    .clk40(clk40),
    .data_in(data_out),
    //.data_in({data_out[0], 64'h7800_0000_0000_0042, data_out[2], data_out[3]}),
    .sync_in(sync_out),
    .blocksync_out(blocksync_out),
    .gearbox_rdy_rx(gearbox_rdy_rx),
    .data_valid(data_valid),
    .data_out_cb(data_out_cb),
    .sync_out_cb(sync_out_cb),
    .data_valid_cb(data_valid_cb),
    .channel_bonded(channel_bonded)
);

endmodule
