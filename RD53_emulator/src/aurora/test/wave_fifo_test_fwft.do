onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_test_tb_fwft/clk40
add wave -noupdate -expand /fifo_test_tb_fwft/data_in
add wave -noupdate -expand /fifo_test_tb_fwft/din
add wave -noupdate -expand /fifo_test_tb_fwft/dout
add wave -noupdate -expand /fifo_test_tb_fwft/rd_en
add wave -noupdate -expand /fifo_test_tb_fwft/wr_en
add wave -noupdate -expand /fifo_test_tb_fwft/cb_detect
add wave -noupdate -expand /fifo_test_tb_fwft/empty
add wave -noupdate /fifo_test_tb_fwft/full
add wave -noupdate /fifo_test_tb_fwft/half_clk40_period
add wave -noupdate /fifo_test_tb_fwft/rst
add wave -noupdate /fifo_test_tb_fwft/sync
add wave -noupdate /fifo_test_tb_fwft/wr_cnt
add wave -noupdate /fifo_test_tb_fwft/din_shift
add wave -noupdate /fifo_test_tb_fwft/wr_en_shift
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb_fwft/fifo_0/wr_en
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb_fwft/fifo_1/wr_en
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb_fwft/fifo_2/wr_en
add wave -noupdate -expand -group {Shifted FIFO wr_en} /fifo_test_tb_fwft/fifo_3/wr_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2230323529 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 170
configure wave -valuecolwidth 249
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {53576250 ps}
