//LFSR and 40-160 cross domain command word FIFO
module cmd_top (
   input  rst, clk40, clk160, 
   input  gen_cmd,
   input  rd_cmd,
   output cmd_valid,
   output [15:0] cmd_data,
   output reg [7:0] cmd_type
);

wire fifo_full, wr_cmd;
wire [15:0] lfsr_data;
wire [10:0] data_count;
reg [15:0] next_cmd;

reg [3:0] state, next_state;

parameter [3:0] NOOP = 4'b0000, ECR_1 = 4'b0001, BCR_1 = 4'b0010,
                GP_1 = 4'b0011, GP_2 = 4'b0100, CAL_1 = 4'b0101,
                CAL_2 = 4'b0110, CAL_3 = 4'b0111, RD_1 = 4'b1000,
                RD_2 = 4'b1001, RD_3 = 4'b1010, WR_1 = 4'b1011,
                WR_2 = 4'b1100, WR_3 = 4'b1101, WR_4 = 4'b1110;


// COMMANDS
// 
// ECR           - 0101_1010_0101_1010
// BCR           - 0101_1001_0101_1001
// GLOBAL PULSE  - 0101_1100_0101_1100 | ID<3:0>, 0, D<4:0>
// CAL           - 0110_0011_0110_0011 | ID<3:0>, D<15>, D<14:10> | D<9:5>, D<4:0> 
// WR_REG        - 0110_0110_0110_0110 | ID<3:0>, 0, A<8:4> | A<3:0>, D<15>, D<14:10> | D<9:5>, D<4:0>
// RD_REG        - 0110_0101_0110_0101 | ID<3:0>, 0, A<8:4> | A<3:0>, 0, 00000 
// NOOP          - 0110_1001_0110_1001
// SYNC          - Sent by the top level module - 1000_0001_0111_1110


always@(posedge clk40 or posedge rst) begin
    if(rst) state <= CAL_1;
    else    state <= next_state;
end

// Send commands in a way that makes sense
always@(*) begin
    case(state) 
        NOOP: begin
            next_cmd = 16'b0110_1001_0110_1001;
            cmd_type = 8'h2D; // '-'
            if(lfsr_data[2:0] == 3'b000)      next_state = ECR_1;
            else if(lfsr_data[2:0] == 3'b001) next_state = BCR_1;
            else if(lfsr_data[2:0] == 3'b010) next_state = CAL_1;
            else if(lfsr_data[2:0] == 3'b011) next_state = GP_1;
            else if(lfsr_data[2:0] == 3'b100) next_state = RD_1;
            else if(lfsr_data[2:0] == 3'b101) next_state = WR_1;
            else                              next_state = NOOP;
        end
        ECR_1: begin
            next_cmd = 16'b0101_1010_0101_1010;
            next_state = NOOP;
            cmd_type = 8'h45; // 'E'
        end
        BCR_1: begin
            next_cmd = 16'b0101_1001_0101_1001;
            next_state = NOOP;
            cmd_type = 8'h42; // 'B'
        end
        GP_1: begin
            next_cmd = 16'b0101_1100_0101_1100;
            next_state = GP_2;
            cmd_type = 8'h50; // 'P'
        end
        GP_2: begin
            next_cmd = 16'b0110_1010_1010_0101;
            next_state = NOOP;
            cmd_type = 8'h50; // 'P'
        end
        CAL_1: begin
            next_cmd = 16'b0110_0011_0110_0011;
            next_state = CAL_2;
            cmd_type = 8'h43; // 'C'
        end
        CAL_2: begin
            next_cmd = 16'b0110_1010_0110_1010;
            next_state = CAL_3;
            cmd_type = 8'h43; // 'C'
        end
        CAL_3: begin
            next_cmd = 16'b0110_1010_1010_0110;
            next_state = NOOP;
            cmd_type = 8'h43; // 'C'
        end 
        RD_1: begin 
            next_cmd = 16'b0110_0101_0110_0101;
            next_state = RD_2; 
            cmd_type = 8'h52; // 'R'
        end
        RD_2: begin
            next_cmd = 16'b0110_1010_0110_1010;
            next_state = RD_3;
            cmd_type = 8'h52; // 'R'
        end
        RD_3: begin 
            next_cmd = 16'b0110_1010_0110_1010;
            next_state = NOOP;
            cmd_type = 8'h52; // 'R'
        end
        WR_1: begin
            next_cmd = 16'b0110_0110_0110_0110;
            next_state = WR_2;
            cmd_type = 8'h57; // 'W'
        end 
        WR_2: begin
            next_cmd = 16'b0110_1010_0110_1010;
            next_state = WR_3;
            cmd_type = 8'h57; // 'W'
        end
        WR_3: begin
            next_cmd = 16'b0110_1010_1101_0100;
            next_state = WR_4;
            cmd_type = 8'h57; // 'W'
        end 
        WR_4: begin
            next_cmd = 16'b1101_0100_1101_0100;
            next_state = NOOP;
            cmd_type = 8'h57; // 'W'
        end
        default: begin next_state = NOOP; next_cmd = 16'b0110_1001_0110_1001; end // NOOP
    endcase
end

// Generate random commands
LFSR lfsr_inst(
   .clk(clk40),
   .rst(rst),
   .gen_cmd(gen_cmd),
   .wr_cmd(wr_cmd),
   .dataout(lfsr_data)
);

// 1024 depth
// Hold commands until they are needed
cmd_fifo cmd_fifo_inst(
    .rst(rst), 
    .wr_clk(clk40), 
    .rd_clk(clk160), 
    .din(next_cmd), 
    .wr_en(wr_cmd),
    .rd_en(rd_cmd), 
    .dout (cmd_data), 
    .full (fifo_full), 
    .empty(), 
    .valid(cmd_valid),
    .rd_data_count(data_count)
);

endmodule
