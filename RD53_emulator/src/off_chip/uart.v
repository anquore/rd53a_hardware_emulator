// UART TX/RX module from FPGA to HOST
// 115200 Baud @ 40Mhz

module uart_tx(
    input clk,
    input TxD_start,
    input [7:0] TxD_data,
    output TxD,
    output TxD_busy
);

// Assert TxD_start for (at least) one clock cycle to start transmission of TxD_data
// TxD_data is latched so that it doesn't have to stay valid while it is being sent

parameter ClkFrequency = 40000000;
parameter Baud = 115200;

wire 	    BitTick;
reg [3:0] 	TxD_state = 0;
reg [7:0] 	TxD_shift = 0;
wire 		TxD_ready = (TxD_state == 0);

BaudTickGen #(
    .ClkFrequency(ClkFrequency), 
    .Baud(Baud)
    ) tickgen ( 
    .clk(clk), 
    .enable(TxD_busy), 
    .tick(BitTick)
);

assign TxD_busy = ~TxD_ready;
assign TxD = (TxD_state<4) | (TxD_state[3] & TxD_shift[0]);

always @(posedge clk) begin
    if(TxD_ready & TxD_start)
        TxD_shift <= TxD_data;
    else
        if(TxD_state[3] & BitTick)
            TxD_shift <= (TxD_shift >> 1);

    case(TxD_state)
        4'b0000: if(TxD_start) TxD_state <= 4'b0100; 
        4'b0100: if(BitTick) TxD_state <= 4'b1000;
        4'b1000: if(BitTick) TxD_state <= 4'b1001;
        4'b1001: if(BitTick) TxD_state <= 4'b1010;
        4'b1010: if(BitTick) TxD_state <= 4'b1011;
        4'b1011: if(BitTick) TxD_state <= 4'b1100;
        4'b1100: if(BitTick) TxD_state <= 4'b1101;
        4'b1101: if(BitTick) TxD_state <= 4'b1110;
        4'b1110: if(BitTick) TxD_state <= 4'b1111;
        4'b1111: if(BitTick) TxD_state <= 4'b0010;
        4'b0010: if(BitTick) TxD_state <= 4'b0011;
        4'b0011: if(BitTick) TxD_state <= 4'b0000;
        default: if(BitTick) TxD_state <= 4'b0000;
    endcase 

end 

endmodule 



module uart_rx(
    input clk,
    input RxD,
    output reg RxD_data_ready = 1'b0,
    output reg [7:0] RxD_data = 1'b0,  // data received, valid only (for one clock cycle) when RxD_data_ready is asserted
    
    output RxD_idle,                // asserted when no data has been received for a while
    output reg RxD_endofpacket = 1'b0  // asserted for one clock cycle when a packet has been detected (i.e. RxD_idle is going high)
);

parameter   ClkFrequency = 40000000;
parameter   Baud = 115200;
parameter   Oversampling = 8;

reg [3:0] 	RxD_state = 0;
reg [1:0] 	Filter_cnt = 2'b11;
reg [1:0] 	RxD_sync = 2'b11;
reg         RxD_bit = 1'b1;
wire 		OversamplingTick;

BaudTickGen #(
    .ClkFrequency(ClkFrequency), 
    .Baud(Baud),
    .Oversampling(Oversampling)
    ) tickgen ( 
    .clk(clk), 
    .enable(1'b1), 
    .tick(OversamplingTick)
);

always @(posedge clk) if(OversamplingTick) RxD_sync <= {RxD_sync[0], RxD};

always @(posedge clk)
    if(OversamplingTick) begin
        if(RxD_sync[1]==1'b1 && Filter_cnt!=2'b11) Filter_cnt <= Filter_cnt + 1'd1;
        else
	       if(RxD_sync[1]==1'b0 && Filter_cnt!=2'b00) Filter_cnt <= Filter_cnt - 1'd1;

        if(Filter_cnt==2'b11) RxD_bit <= 1'b1;
        else
            if(Filter_cnt==2'b00) RxD_bit <= 1'b0;
   end // if (OversamplingTick)

function integer log2(input integer v); begin 
    log2=0;
    while(v>>log2) log2=log2+1;
end 
endfunction

localparam l2o = log2(Oversampling);

reg [l2o-2:0] OversamplingCnt = 0;
wire 	      sampleNow = OversamplingTick && (OversamplingCnt == (Oversampling / 2) - 1);

always @(posedge clk) 
    if(OversamplingTick) OversamplingCnt <= (RxD_state==0) ? 1'd0 : OversamplingCnt + 1'd1;

// Aumulate the RxD bits in a shift-register
always @(posedge clk)
    case(RxD_state)
        4'b0000: if(~RxD_bit) RxD_state <= `ifdef SIMULATION 4'b1000 `else 4'b0001 `endif;
        4'b0001: if(sampleNow) RxD_state <= 4'b1000;
        4'b1000: if(sampleNow) RxD_state <= 4'b1001;
        4'b1001: if(sampleNow) RxD_state <= 4'b1010;
        4'b1010: if(sampleNow) RxD_state <= 4'b1011;
        4'b1011: if(sampleNow) RxD_state <= 4'b1100;
        4'b1100: if(sampleNow) RxD_state <= 4'b1101;
        4'b1101: if(sampleNow) RxD_state <= 4'b1110;
        4'b1110: if(sampleNow) RxD_state <= 4'b1111;
        4'b1111: if(sampleNow) RxD_state <= 4'b0010;
        4'b0010: if(sampleNow) RxD_state <= 4'b0000;
        default: RxD_state <= 4'b0000;
    endcase // case (RxD_state)

always @(posedge clk)
    if(sampleNow && RxD_state[3]) RxD_data <= {RxD_bit, RxD_data[7:1]};

always @(posedge clk) begin
	RxD_data_ready <= (sampleNow && RxD_state==4'b0010 && RxD_bit);
end

reg [l2o+1:0] GapCnt = 0;

always @(posedge clk) 
    if (RxD_state!=0) GapCnt<=0;
    else if(OversamplingTick & ~GapCnt[log2(Oversampling)+1]) GapCnt <= GapCnt + 1'h1;

assign RxD_idle = GapCnt[l2o+1];

always @(posedge clk) 
    RxD_endofpacket <= OversamplingTick & ~GapCnt[l2o+1] & &GapCnt[l2o:0];

endmodule

// Tick generation used for triggering UART modules
module BaudTickGen(
    input clk, enable,
    output tick  // generate a tick at the specified baud rate * oversampling
);

parameter ClkFrequency = 40000000; //40 MHz
parameter Baud = 115200;
parameter Oversampling = 1;

function integer log2(input integer v); begin 
    log2=0;
    while(v>>log2) log2=log2+1;
end 
endfunction

localparam AccWidth = log2(ClkFrequency/Baud)+8;
    
reg [AccWidth:0] Acc = 0;

localparam ShiftLimiter = log2(Baud*Oversampling >> (31-AccWidth));
localparam Inc = ((Baud*Oversampling << (AccWidth-ShiftLimiter))+(ClkFrequency>>(ShiftLimiter+1)))/(ClkFrequency>>ShiftLimiter);

always @(posedge clk) 
    if(enable) Acc <= Acc[AccWidth-1:0] + Inc[AccWidth:0];
    else Acc <= Inc[AccWidth:0];

assign tick = Acc[AccWidth];

endmodule // BaudTickGen
